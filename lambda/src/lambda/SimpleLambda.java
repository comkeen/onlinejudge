package lambda;

public class SimpleLambda {
	public static void main(String[] args) {
		Runnable lambda = () -> System.out.println(1);
		lambda.run();
	}
}
