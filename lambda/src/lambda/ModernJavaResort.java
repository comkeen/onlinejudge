package lambda;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ModernJavaResort implements IResortService {
	
	private GuestRepository repository;
	
	public ModernJavaResort(GuestRepository repository) {
		this.repository = repository;
	}

	public List<String> findGuestNamesByCompany(String company) {
		List<Guest> guests = repository.findAll();
		return guests.stream()
			.filter(g -> company.equals(g.getCompany()))
			.sorted(Comparator.comparing(g -> g.getGrade()))
			.map(g -> g.getName())
			.collect(Collectors.toList());
	}
}
