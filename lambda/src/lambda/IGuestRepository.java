package lambda;

import java.util.List;

public interface IGuestRepository {
	public void save(Guest ... guest);
	public List<Guest> findAll();
	public void deleteAll();
}
