package lambda;

import java.util.List;

public interface IResortService {
	public List<String> findGuestNamesByCompany(String company);
}
