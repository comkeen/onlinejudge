package algorithm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class Algorithm
{

	@Test
	public void FizzBuzzTest()
	{
		final List<String> toReturn = new ArrayList<>();
		final int n = 1000;
		
		for(int i = 1; i <= n; i++)
		{
			final String word = toWord(i, 3, "Fizz") + toWord(i, 5, "Buzz");
			if(word.length() == 0)
			{
				toReturn.add(Integer.toString(i));
			}
			else
			{
				toReturn.add(word);
			}
		}
//		System.out.println(toReturn);
	}

//	@Test
	public void FibonacciTest()
	{
		final long nonCachedStart = System.nanoTime();
		Assert.assertEquals(1134903170, fibN(45));
		final long nonCachedFinish = System.nanoTime();
		Assert.assertEquals(1134903170, cachedFibN(45));
		final long cachedFinish = System.nanoTime();

		System.out.printf("Non cached time : %d nonoseconds\n", nonCachedFinish - nonCachedStart);
		System.out.printf("Cached time : %d nonoseconds\n", cachedFinish - nonCachedFinish);
	}

//	@Test
	public void FactorialTest()
	{
		final int n = 4;
		final BigInteger bi = factorial(n);
		System.out.println(bi);
	}

	@Test
	public void AnagramsTest()
	{
		final Map<String, List<String>> lookup = new HashMap<>();
	}

	private BigInteger factorial(final int n)
	{
		BigInteger bi = BigInteger.valueOf(1);
		
		for(int i = n; i >= 1; i--)
		{
			bi = bi.multiply(BigInteger.valueOf(i));
		}
		return bi;
	}

	private BigInteger factorial(BigInteger bi)
	{
		if(bi.compareTo(BigInteger.ONE) == 0)
		{
			return bi;
		}
		return bi.multiply(factorial(bi.subtract(BigInteger.ONE)));
	}

	private List<Integer> createFibonacci(int n)
	{
		final List<Integer> fibonacciSeq = new ArrayList<>();

		if(n == 0)
		{
			System.out.println("not valid range");
			return fibonacciSeq;
		}
		fibonacciSeq.add(0);
		if(n == 1)
		{
			return fibonacciSeq;
		}
		fibonacciSeq.add(1);
		if(n == 2)
		{
			return fibonacciSeq;
		}
		
		for(int i = 2; i < n; i++)
		{
			int left = fibonacciSeq.get(i-2);
			int right = fibonacciSeq.get(i-1);
			fibonacciSeq.add(left+right);
		}
		return fibonacciSeq;
	}

	private int fibN(int n)
	{
		if(n == 0)
		{
			return 0;
		}
		if(n == 1)
		{
			return 1;
		}
		return fibN(n-2) + fibN(n-1);
	}


	Map<Integer, Integer> fibCache;
	private int cachedFibN(int n)
	{
		fibCache = new HashMap<>();
		fibCache.put(0, 0);
		fibCache.put(1, 1);
		return recursiveCachedFibN(n);
	}

	private int recursiveCachedFibN(int n)
	{
		if(fibCache.containsKey(n))
		{
			return fibCache.get(n);
		}
		int value = recursiveCachedFibN(n-1) + recursiveCachedFibN(n-2);
		fibCache.put(n, value);
		return value;
	}

	private String toWord(final int target, final int divisor, String string)
	{
		return target%divisor == 0 ? string : "";
	}

}
