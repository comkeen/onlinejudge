package sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class SortTester
{
	final int[] numArr = {5, 4, 7, 3, 9, 6, 10, 1, 2, 8};
	final int[] expectedArr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	final List<Integer> numList = new ArrayList<>(Arrays.asList(5,4,7,3,9,6,10,1,2,8));
	final List<Integer> expectedList = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
	
	@Test
	public void bubbleSortTest()
	{
		int[] nums = numArr;

		boolean switched;
		do
		{
			switched = false;
			for(int i = 0; i < nums.length - 1; i++)
			{
				if(nums[i] > nums[i+1])
				{
					int temp = nums[i];
					nums[i] = nums[i+1];
					nums[i+1] = temp;
					switched = true;
				}
			}
		}
		while(switched);
		Assert.assertArrayEquals(nums, expectedArr);
	}
	
	@Test
	public void bubbleListSortTest()
	{
		List<Integer> nums = numList;

		boolean switched;
		do
		{
			switched = false;
			for(int i = 0; i < nums.size()-1; i++)
			{
				if(nums.get(i) > nums.get(i+1))
				{
					int temp = nums.get(i+1);
					nums.remove(i+1);
					nums.add(i, temp);
					switched = true;
				}
			}
		}
		while(switched);
		Assert.assertEquals(nums, expectedList);
	}

	@Test
	public void insertSortTest()
	{
		List<Integer> nums = numList;

		List<Integer> sorted = new LinkedList<>();
		originalList: for(Integer num :nums)
		{
			for(int i = 0; i < sorted.size(); i++)
			{
				if(num < sorted.get(i))
				{
					sorted.add(i, num);
					continue originalList;
				}
			}
			sorted.add(sorted.size(), num);
		}
		
		Assert.assertEquals(sorted, expectedList);
	}

	@Test
	public void quickSortTest()
	{
		List<Integer> nums = numList;
		
		List<Integer> sorted = quickSort(nums);		
		Assert.assertEquals(sorted, expectedList);
	}

	@Test
	public void mergeSortTest()
	{
		List<Integer> nums = numList;

		List<Integer> sorted = mergeSort(nums);
		Assert.assertEquals(sorted, expectedList);
	}
	
	@Test
	public void binarySearchTest()
	{
		final List<Integer> list = expectedList;
		final int target = 2;
		Assert.assertTrue(binarySearch(list, target));
	}

	private boolean binarySearch(final List<Integer> list, final int target)
	{
		if(list.isEmpty())
		{
			return false;
		}
		
		final int comparison = list.get(list.size()/2);
		if(target == comparison)
		{
			return true;
		}
		else if(target < comparison)
		{
			return binarySearch(list.subList(0, list.size()/2), target);
		}
		else
		{
			return binarySearch(list.subList(list.size()/2, list.size()), target);
		}
	}

	private List<Integer> mergeSort(final List<Integer> list)
	{
		if(list.size() < 2)
		{
			return list;
		}

		List<Integer> left = list.subList(0, list.size()/2);
		List<Integer> right = list.subList(list.size()/2, list.size());

		List<Integer> sortedLeft = mergeSort(left);
		List<Integer> sortedRight = mergeSort(right);

		return merge(sortedLeft, sortedRight);
	}

	private List<Integer> merge(final List<Integer> left, final List<Integer> right)
	{
		int leftPointer = 0;
		int rightPointer = 0;
		List<Integer> mergedList = new ArrayList<>();

		while(leftPointer < left.size() && rightPointer < right.size())
		{
			int leftTemp = left.get(leftPointer);
			int rightTemp = right.get(rightPointer);
			if(leftTemp < rightTemp)
			{
				mergedList.add(leftTemp);
				leftPointer++;
			}
			else
			{
				mergedList.add(rightTemp);
				rightPointer++;
			}
		}

		if(leftPointer == left.size())
		{
			mergedList.addAll(right.subList(rightPointer, right.size()));
		}		
		if(rightPointer == right.size())
		{
			mergedList.addAll(left.subList(leftPointer, left.size()));	
		}

		return mergedList;
	}

	private List<Integer> quickSort(final List<Integer> nums)
	{
		if(nums.size() < 2)
		{
			return nums;
		}

		int pivot = nums.get(0);
		List<Integer> lower = new ArrayList<>();
		List<Integer> higher = new ArrayList<>();

		for(int i = 1; i < nums.size(); i++)
		{
			int temp = nums.get(i);
			if(temp < pivot)
			{
				lower.add(temp);
			}
			else
			{
				higher.add(temp);
			}
		}

		List<Integer> sorted = quickSort(lower);
		sorted.add(pivot);
		sorted.addAll(quickSort(higher));
		return sorted;
	}
}
