package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

class Main
{
	public Main() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			List<String> inputList = new ArrayList<>();
			String temp = br.readLine();
			while(temp != null && temp.length() != 0) {
				if("42".equals(temp)) {
					break;
				}
				inputList.add(temp);
				temp = br.readLine();
			}
			for(int i = 0; i < inputList.size(); i++) {
				System.out.println(inputList.get(i));
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	public static void main (String[] args) throws java.lang.Exception
	{
		new Main();
	}
}