package numberTheory.PRIME1;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public Main() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			int t = Integer.parseInt(br.readLine());
			int[] inputs = new int[t*2];
			for(int i = 0; i < t*2;) {
				String[] tokens = br.readLine().split("\\s");
				inputs[i++] = Integer.parseInt(tokens[0]);
				inputs[i++] = Integer.parseInt(tokens[1]);
			}
			br.close();
			isr.close();

			for(int i=0; i<t; i++) {
				printPrimes_b(inputs[i*2], inputs[(i*2)+1]);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void printPrimes_b(int from, int to) {
		try {
			BufferedOutputStream bos = new BufferedOutputStream(System.out);
			prime:for(int i=from; i<=to; i++) {
				for(int j=2; j*j<=i; j++) {
					if(i%j==0) {
						continue prime;
					}
				}
				if(i!=1) {
					bos.write((i+"\n").getBytes());
				}
			}
			bos.write(("\n").getBytes());
			bos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new Main();
	}
}
