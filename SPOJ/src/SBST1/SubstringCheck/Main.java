package SBST1.SubstringCheck;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	public Main() {
		try {
			final InputStreamReader isr = new InputStreamReader(System.in);
			final BufferedReader br = new BufferedReader(isr);

			final int t = 24;
			final String[] inputs = new String[t];
			for(int i=0; i<t; i++) {
				inputs[i] = br.readLine();
			}
			br.close();
			isr.close();

			for(int i=0; i<t; i++) {
				checkSubString(inputs[i]);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void checkSubString(final String string) {
		String[] tokens = string.split("\\s");
		Pattern p = Pattern.compile(tokens[1]);
		Matcher m = p.matcher(tokens[0]);
		if(m.find()) {
			System.out.println(1);
		} else {
			System.out.println(0);
		}
	}

	public static void main(String[] args) {
		new Main();
	}
}
