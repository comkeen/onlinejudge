import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
 
public class Main {
     
    private boolean[] isVisit;
     
    public static void main(String[] args) {
        new Main();
    }
 
    private int _F;
    private int _S;
    private int _G;
    private int _U;
    private int _D;
    private int[] button;
     
    public Main() {
        init();
         
        isVisit = new boolean[_F+1];
 
        button = new int[_F+1];
        Queue<Integer> queue = new LinkedList<Integer>();
         
        isVisit[_S] = true; 
         
        queue.offer(_S);
         
        bfs(_S, isVisit, queue);
         
        if(isVisit[_G]){
            System.out.println(button[_G]);
        } else {
            System.out.println("use the stairs");
        }
         
    }
 
 
    private void bfs(int i, boolean[] isVisit, Queue<Integer> queue) {
         
            while(!queue.isEmpty()){
                 
                int temp = queue.peek();
                queue.poll();
                System.out.println(temp);
                if(temp == _G){
                    break;
                }
                 
                if((temp+_U) <= _F && !isVisit[temp+_U]){
                    queue.offer(temp+_U);
                    button[temp+_U] = button[temp] + 1;
                    isVisit[temp+_U] = true;
                }
                 
                if((temp-_D) >= 1 && !isVisit[temp-_D]){
                    queue.offer(temp-_D);
                    button[temp-_D] = button[temp] + 1;
                    isVisit[temp-_D] = true;
                }
            }
    }
 
    public void init() {
        Scanner scanner = new Scanner(System.in);
        _F = scanner.nextInt();
        _S = scanner.nextInt();
        _G = scanner.nextInt();
        _U = scanner.nextInt();
        _D = scanner.nextInt();
        scanner.close();
    }
     
}