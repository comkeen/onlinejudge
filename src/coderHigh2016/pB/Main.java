package coderHigh2016.pB;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public static final String EMPTY = ".";
	public static final String MOUNTIN = "#";
	public static final String HIGH = "-";
	public static final String TURNNEL = "*";
	public static final String LEG = "|";
	
	int N, M, X;
	String[][] map;
	
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String[] top = reader.readLine().split(" ");
			N = Integer.parseInt(top[0]);
			M = Integer.parseInt(top[1]);
			X = Integer.parseInt(top[2])-1;
			
			String[] mountin = reader.readLine().split(" ");
			reader.close();
			map = new String[N][M];
			for(int i = 0; i < M; i++)
			{
				for(int j = 0; j < N; j++)
				{
					map[j][i] = EMPTY;
				}
			}
					
			for(int i = 0; i < M; i++)
			{
				for(int j = 0; j < N; j++)
				{
					int mountinHigh = Integer.parseInt(mountin[i]) - 1;
					if(mountinHigh >= j)
					{
						map[j][i] = MOUNTIN;
					}
					if(mountinHigh >= X)
					{
						if(X == j)
						{
							map[j][i] = TURNNEL;
						}
					}
					else
					{
						if(X == j)
						{
							map[j][i] = HIGH;
						}
						if((i+1)%3 == 0 && X > j && j > mountinHigh)
						{
							map[j][i] = LEG;
						}
					}
					
				}
			}
			printMap();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void printMap()
	{
		for(int j = N-1; j >= 0; j--)
		{
			for(int i = 0; i < M; i++)
			{
				System.out.print(map[j][i]);
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public static void main(String[] args)
	{
		new Main();
	}
}
