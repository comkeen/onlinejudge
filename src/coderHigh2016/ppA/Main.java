package coderHigh2016.ppA;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			int T = Integer.parseInt(reader.readLine());

			String[] inputs = new String[T];
			for(int i = 0; i < T; i++)
			{
				inputs[i] = reader.readLine();
			}
			reader.close();

			BST bst = new BST();
			for(int i = 1; i <= 1023; i++)
			{
				bst.add(i);
			}


			for(int i = 0; i < T; i++)
			{
				String[] tokens = inputs[i].split(" ");
				solve(tokens[0], tokens[1]);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void solve(String string, String string2)
	{
		// TODO Auto-generated method stub

	}

	public static void main(String[] args)
	{
		new Main();
	}

	public class TreeNode
	{
		public int data;
		TreeNode left;
		TreeNode right;

		public TreeNode(int data)
		{
			this.data = data;
			this.left = null;
			this.right = null;
		}
	}

	public class BST
	{
		private TreeNode root;

		public void add(int data)
		{
			if(root == null)
			{
				TreeNode newNode = new TreeNode(data);
				this.root = newNode;
			}

			TreeNode node = root;			
			while(true)
			{
				if(node.left == null)
				{
					TreeNode newNode = new TreeNode(data);
					node.left = newNode;
					return;
				}
				else
				{
					if(node.right == null)
					{
						TreeNode newNode = new TreeNode(data);
						node.right = newNode;
						return;
					}
					else
					{
						node = node.left;
					}
					
				}
			}
		}
	}
}
