package coderHigh2016.pA;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String[] people = reader.readLine().split(" ");
			String[] apple = reader.readLine().split(" ");
			reader.close();
			
			for(int i = 0; i < people.length; i++)
			{
				if(apple[0].equals(people[i]))
				{
					System.out.println(i+1);
					return;
				}
			}
			System.out.println("0");

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
