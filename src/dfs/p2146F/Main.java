package dfs.p2146F;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main
{
	public static final String ONE = "1";
	public static final int[] nx = {1, 0, -1, 0};
	public static final int[] ny = {0, 1, 0, -1};

	private int N;
	private int[][] originalMap;
	private int[][] map;

	public Main()
	{
		try
		{
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			String top = br.readLine();
			N = Integer.parseInt(top);
			String[] inputs = new String[N];
			for(int i = 0; i < N; i++)
			{
				inputs[i] = br.readLine();
			}
			br.close();
			isr.close();

			originalMap = new int[N][N];
			for(int i = 0; i < N; i++)
			{
				String[] temps = inputs[i].split(" ");

				for(int j = 0; j < N; j++)
				{
					if(ONE.equals(temps[j]))
					{
						originalMap[i][j] = 1;
					}
				}
			}

			int max = initIslands();

			bfs(max);
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}

	public void test()
	{
		String top = "10";
		N = Integer.parseInt(top);

		String[] inputs = new String[N];
		inputs[0] = "1 1 1 0 0 0 0 1 1 1";
		inputs[1] = "1 1 1 1 0 0 0 0 1 1";
		inputs[2] = "1 0 1 1 0 0 0 0 1 1";
		inputs[3] = "0 0 1 1 1 0 0 0 0 1";
		inputs[4] = "0 0 0 1 0 0 0 0 0 1";
		inputs[5] = "0 0 0 0 0 0 0 0 0 1";
		inputs[6] = "0 0 0 0 0 0 0 0 0 0";
		inputs[7] = "0 0 0 0 1 1 0 0 0 0";
		inputs[8] = "0 0 0 0 1 1 1 0 0 0";
		inputs[9] = "0 0 0 0 0 0 0 0 0 0";

		originalMap = new int[N][N];
		for(int i = 0; i < N; i++)
		{
			String[] temps = inputs[i].split(" ");

			for(int j = 0; j < N; j++)
			{
				if(ONE.equals(temps[j]))
				{
					originalMap[i][j] = 1;
				}
			}
		}

		int max = initIslands();

		bfs(max);
	}

	private void bfs(int max)
	{
		int min = 987654321;
		bfs:for(int islandNum = 1; islandNum <= max; islandNum++)
		{
			int depth = -1;
			int[][] temp = map;
			while(true)
			{
				int[][] expandedTemp = new int[N][N];
				for(int i = 0; i < N; i++)
				{
					for(int j = 0; j < N; j++)
					{
						if(temp[i][j] == islandNum)
						{
							expandedTemp[i][j] = temp[i][j];
							if(map[i][j] > 0 && map[i][j] != islandNum)
							{
								if(min > depth)
								{
									min = depth;
								}
								continue bfs;
							}
							for(int n = 0; n < 4; n++)
							{
								if(i+ny[n] >= 0 && i+ny[n] < N && j+nx[n] >= 0 && j+nx[n] < N)
								{
									if(expandedTemp[i+ny[n]][j+nx[n]] == 0)
									{
										expandedTemp[i+ny[n]][j+nx[n]] = islandNum;
									}
								}
							}
						}
					}
				}
				temp = expandedTemp;
				depth++;
			}
		}
		System.out.println(min);
	}

	private void printArr(int[][] temp)
	{
		for(int i = 0; i < temp.length; i++)
		{
			System.out.println(Arrays.toString(temp[i]));
		}
		System.out.println();
	}

	private int initIslands()
	{
		int islandNum = 0;
		map = new int[N][N];
		for(int i = 0; i < N; i++)
		{
			check:for(int j = 0; j < N; j++)
			{
				if(originalMap[i][j] > 0)
				{
					for(int n = 0; n < 4; n++)
					{
						if(i+ny[n] >= 0 && i+ny[n] < N && j+nx[n] >= 0 && j+nx[n] < N)
						{
							if(map[i+ny[n]][j+nx[n]] > 0)
							{
								int num = map[i+ny[n]][j+nx[n]];
								map[i][j] = num;
								continue check;
							}
						}
					}
					map[i][j] = ++islandNum;
				}
			}
		}
		mergeIsland();
		return islandNum;
	}

	private void mergeIsland()
	{
		for(int i = 0; i < map.length; i++)
		{
			for(int j = 0; j < map[0].length; j++)
			{
				if(map[i][j] > 0)
				{
					int islandNum = map[i][j];
					for(int n = 0; n < 4; n++)
					{
						if(i+ny[n] >= 0 && i+ny[n] < N && j+nx[n] >= 0 && j+nx[n] < N)
						{
							int nextIslandNum = map[i+ny[n]][j+nx[n]];
							if(nextIslandNum > 0)
							{
								if(islandNum < nextIslandNum)
								{
									map[i+ny[n]][j+nx[n]] = islandNum;
								}
							}
						}
					}
				}
			}
		}
	}
}
