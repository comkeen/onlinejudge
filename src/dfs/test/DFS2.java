package dfs.test;

import java.util.Scanner;

public class DFS2
{
	private Scanner scanner;

	private int max, min;
	int[][] map;

	public DFS2()
	{
		scanner = new Scanner(System.in);

		init();

		scanner.close();
		return;
	}

	private void init()
	{
		map = new int[32][32];

		max = scanner.nextInt();
		min = max * max; // 모든 경로를 돌아다녀도 n * n이니, 이를 최소값으로 지정해둔다

		for (int i = 0; i < max; i++)
		{
			for (int j = 0; j < max; j++)
			{
				map[i][j] = scanner.nextInt();
			}
		}
		DFS(0, 0, 1); // DFS 시작!

		System.out.printf("최단 거리: %d\n", min);

	}
	private void DFS(int x, int y, int l)
	{		
		// 도착 지점에 도착했을 경우
		if (x == max - 1 && y == max - 1)
		{
			// 현재 최소값이 l보다 크면, l이 작은 것이므로 l를 최소값으로 지정
			if (min > l)
			{
				min = l;
			}
			return;
		}
		map[y][x] = 0; // 방문했음을 표시하기 위해 0을 대입

		// 위로 이동할 수 있다면 이동!
		if (y > 0 && map[y - 1][x] != 0)
		{
			DFS(x, y - 1, l + 1);
		}
		// 아래로 이동할 수 있다면 이동!
		if (y < max - 1 && map[y + 1][x] != 0) 
		{
			DFS(x, y + 1, l + 1);
		}
		// 왼쪽으로 이동할 수 있다면 이동!
		if (x > 0 && map[y][x - 1] != 0) 
		{
			DFS(x - 1, y, l + 1);
		}
		// 오른쪽으로 이동할 수 있다면 이동!
		if (x < max - 1 && map[y][x + 1] != 0) 
		{
			DFS(x + 1, y, l + 1);
		}

		map[y][x] = 1; // 지나간 자리를 원상태로 되돌리기 위해 1을 대입
	}

	public static void main(String[] args)
	{
		new DFS2();
	}
}
