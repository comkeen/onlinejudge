package dfs.test;

import java.util.Scanner;

public class DFS1
{
	private Scanner scanner;
	
	private int max, start;
	int[][] map;
	int[] visit;

	public DFS1()
	{
		scanner = new Scanner(System.in);

		init();
		
		scanner.close();
		return;
	}

	private void init()
	{
		visit = new int[32];
		map = new int[32][32];
		
		max = scanner.nextInt();
		start = scanner.nextInt();
		
		while(true)
		{
			int v1 = scanner.nextInt();
			int v2 = scanner.nextInt();
			if(v1 == -1 && v2 == -1)
			{
				break;
			}
			map[v1][v2] = 1;
			map[v2][v1] = 1;
		}		
		DFS(start);
		printArray(visit);
	}
	private void DFS(int v)
	{		
		int i;
		 
	    visit[v] = 1; // 정점 v를 방문했다고 표시
	    for (i = 1; i <= max; i++) 
	    {
	        // 정점 v와 정점 i가 연결되었고, 정점 i를 방문하지 않았다면
	        if (map[v][i] == 1 && visit[i] == 0)
	        {
	            System.out.printf("%d에서 %d로 이동\n", v, i);
	            
	            // 정점 i에서 다시 DFS를 시작한다
	            DFS(i);
	        }
	    }
	}
	
	private void printArray(int[] input)
	{
		for(int i = 0; i < input.length; i++)
		{
			System.out.printf("%2d ", input[i]);
		}
		System.out.println();
	}
	
	public static void main(String[] args)
	{
		new DFS1();
	}
}
