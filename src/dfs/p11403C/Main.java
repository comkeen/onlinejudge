package dfs.p11403C;

import java.util.Scanner;

public class Main
{
	private Scanner scanner;
	
	int[][] adMatrix;
	int[][] result;

	public Main()
	{
		scanner = new Scanner(System.in);

		int n = scanner.nextInt();
		adMatrix = new int[n][n];
		result = new int[n][n];
		
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
			{
				adMatrix[i][j] = scanner.nextInt();
			}
		}
		scanner.close();
		
		solve(n);
		return;
	}

	private void solve(int n)
	{		
		for(int i = 0; i < n; i++)
		{
			int[] visit = new int[n];
			DFS(i, visit, true);
			result[i] = visit;
		}
		printResult(result);
	}
	private void DFS(int i, int[] visit, boolean isFirst)
	{		
		if(!isFirst)
		{
		    visit[i] = 1; // 정점 v를 방문했다고 표시
		}
	    for (int j = 0; j < adMatrix[0].length; j++)
	    {
	        if (adMatrix[i][j] == 1 && visit[j] == 0)
	        {
	            DFS(j, visit, false);
	        }
	    }
	}
	
	private void printResult(int[][] result)
	{
		for(int i = 0; i < result.length; i++)
		{
			for(int j = 0; j < result[0].length; j++)
			{
				System.out.print(result[i][j]);
				System.out.print(" ");
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args)
	{
		new Main();
	}
}
