package dfs.p5021;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

public class Map {

	private HashMap<String, Person> familyMap;


	@Test
	public void test() {

		String input = "9 2\nedwardi\ncharlesi edwardi diana\nphilip charlesi mistress\nwilhelm mary philip\nmatthew wilhelm helen\nedwardii charlesi laura\nalice laura charlesi\nhelen alice bernard\nhenrii edwardii roxane\ncharlesii elizabeth henrii\ncharlesii\nmatthew";
		String[] lines = input.split("\n");
		String[] tokens = lines[0].split(" ");
		int N = Integer.parseInt(tokens[0]);
		int M = Integer.parseInt(tokens[1]);
		String kingName = lines[1];

		String[] families = new String[N];
		for(int i = 0; i < N; i++) {
			families[i] = lines[2+i];
		}
		String[] candidates = new String[M];
		for(int i = 0; i < M; i++) {
			candidates[i] = lines[2+N+i];
		}
		findSuccessor_map(kingName, families, candidates);
	}

	private void findSuccessor_map(String kingName, String[] families, String[] candidates) {
		familyMap = new HashMap<>();
		Person king = new Person(kingName);
		king.bloodRate = 1.0f;
		familyMap.put(kingName, king);

		//build familyMap
		for(int i = 0; i < families.length; i++) {
			String[] names = families[i].split(" ");
			
			if(!familyMap.containsKey(names[0])) {
				familyMap.put(names[0], new Person(names[0]));
			}
			Person person = familyMap.get(names[0]);
			
			if(!familyMap.containsKey(names[1])) {
				familyMap.put(names[1], new Person(names[1]));
			}
			Person leftParent = familyMap.get(names[1]);
			leftParent.children.add(person);
			
			if(!familyMap.containsKey(names[2])) {
				familyMap.put(names[2], new Person(names[2]));
			}
			Person rightParent = familyMap.get(names[2]);
			rightParent.children.add(person);
			
			person.leftParent = leftParent;
			person.rightParent = rightParent;
			
			person.bloodRate = leftParent.bloodRate/2 + rightParent.bloodRate/2;
			updateBloodRate_dfs(person);
			
			familyMap.put(names[0], person);
		}

		//find successor
		float max = 0.0f;
		String successor = "";
		for(int i = 0; i < candidates.length; i++) {
			Person candidate = familyMap.get(candidates[i]);
			if(max < candidate.bloodRate) {
				max = candidate.bloodRate;
				successor = candidate.name;
			}
		}
		System.out.println(successor);
	}

	private void updateBloodRate_dfs(Person temp) {

		if(temp.children.size() == 0) {
			return;
		}
		for (Person child : temp.children) {
			child.bloodRate = child.leftParent.bloodRate/2 + child.rightParent.bloodRate/2;
			updateBloodRate_dfs(child);
		}
	}


	public class Person {

		public Person leftParent;
		public Person rightParent;
		public List<Person> children;

		public String name;
		public float bloodRate;

		public Person(String name) {
			this.leftParent = null;
			this.rightParent = null;
			this.children = new ArrayList<>();

			this.name = name;
			this.bloodRate = 0.0f;
		}
		
		public void printPerson() {
			System.out.print("name,blood,ch:" + name + "," + bloodRate);
			for(int i = 0; i < children.size(); i++) {
				System.out.print(" " + children.get(i).name);
			}
			System.out.println();
		}
	}
}
