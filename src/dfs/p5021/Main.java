package dfs.p5021;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


public class Main {

	private Map<String, Person> familyMap;
	private String kingName;
	private Map<String, Double> bloodMap;


	public static void main(String[] args) {

		new Main();
	}

	public Main() {

		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			String[] tokens = br.readLine().split(" ");
			int N = Integer.parseInt(tokens[0]);
			int M = Integer.parseInt(tokens[1]);
			kingName = br.readLine();

			String[] families = new String[N];
			for(int i = 0; i < N; i++) {
				families[i] = br.readLine();
			}
			String[] candidates = new String[M];
			for(int i = 0; i < M; i++) {
				candidates[i] = br.readLine();
			}
			br.close();
			isr.close();

			findSuccessor_map(families, candidates);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void findSuccessor_map(String[] families, String[] candidates) {
		familyMap = new HashMap<>();
		bloodMap = new HashMap<>();

		for(int i = 0; i < families.length; i++) {
			String[] names = families[i].split(" ");
			familyMap.put(names[0], new Person(names[1], names[2]));
		}

		double max = 0;
		String successor = "";
		for(int i = 0; i < candidates.length; i++) {
			double bloodRate = dfs(candidates[i]);
			if(max < bloodRate) {
				max = bloodRate;
				successor = candidates[i];
			}
		}
		System.out.println(successor);
	}

	private double dfs(String name) {
		
		double bloodRate = 0;
		if(kingName.equals(name)) {
			return 1;
		}
		if(!familyMap.containsKey(name)) {
			return 0;
		}
		
		Person person = familyMap.get(name);
		if(bloodMap.containsKey(name)) {
			return bloodMap.get(name);
		}
		bloodRate = dfs(person.leftParent)/2 + dfs(person.rightParent)/2;
		bloodMap.put(name, bloodRate);
		
		return bloodRate;
	}

	public class Person {

		public String leftParent;
		public String rightParent;

		public Person(String leftParent, String rightParent) {
			this.leftParent = leftParent;
			this.rightParent = rightParent;
		}
	}
}
