package dfs.p1707C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Main
{
	public Main()
	{	
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			int k = Integer.parseInt(top);

			List<List<List<Integer>>> adjs = new ArrayList<>();
			for(int i = 0; i < k; i++)
			{
				String[] temp = reader.readLine().split(" ");
				int v = Integer.parseInt(temp[0]);
				int e = Integer.parseInt(temp[1]);

				List<List<Integer>> adj = new ArrayList<List<Integer>>();
				for(int j = 0; j < v; j++)
				{
					List<Integer> list = new ArrayList<>();
					adj.add(list);
				}
				for(int j = 0; j < e; j++)
				{
					String[] raw = reader.readLine().split(" ");
					int from = Integer.parseInt(raw[0]) - 1;
					int to = Integer.parseInt(raw[1]) - 1;

					adj.get(from).add(to);
					adj.get(to).add(from);
				}
				adjs.add(adj);
			}
			reader.close();

			for(int i = 0; i < adjs.size(); i++)
			{
				if(solve_c(adjs.get(i)))
				{
					System.out.println("YES");
				}
				else
				{
					System.out.println("NO");
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private boolean solve_c(List<List<Integer>> adjList)
	{
		int size = adjList.size();
		Queue<Integer> queue = new LinkedList<Integer>();
		int[] visit = new int[size];

		for(int a = 0; a < size; a++)
		{
			int pos = a;
			if(visit[pos] != 0)
			{
				continue;
			}
			queue.clear();
			visit[pos] = 1;
			queue.offer(pos);

			while(!queue.isEmpty())
			{
				pos = queue.poll();
				List<Integer> nexts = adjList.get(pos);
				for(int i = 0; i < nexts.size(); i++)
				{
					int next = nexts.get(i);
					if(visit[next] == 0)
					{
						visit[next] = -visit[pos];
						queue.offer(next);
					}
					else
					{
						if(visit[next] == visit[pos])
						{
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
