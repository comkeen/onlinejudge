package dp.p2579C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public static final int INF = Integer.MAX_VALUE;
	
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			int cases = Integer.parseInt(top);

			int[] inputs = new int[cases];
			for(int i = 0; i < cases; i++)
			{
				inputs[i] = Integer.parseInt(reader.readLine());
			}
			reader.close();
			
			solve(inputs);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(int[] input)
	{
		int[][] dp = new int[300][2];
		
		dp[0][0] = input[0];
		dp[0][1] = 0;
		dp[1][0] = input[1];
		dp[1][1] = dp[0][0] + input[1];
		for(int i = 2; i < input.length; i++)
		{
			dp[i][0] = Math.max(dp[i-2][0], dp[i-2][1]) + input[i];
			dp[i][1] = dp[i-1][0] + input[i];
		}
		System.out.println(Math.max(dp[input.length-1][0], dp[input.length-1][1]));
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
