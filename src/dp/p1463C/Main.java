package dp.p1463C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public static final int MAX = Integer.MAX_VALUE/2;
	
	int[] result;
	
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			int n = Integer.parseInt(top);
			reader.close();
			
			result = new int[n+1];
			int min = solve(n);
			System.out.println(min);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private int solve(int input)
	{
		int count1 = MAX;
		int count2 = MAX;
		int count3 = MAX;
		if(input == 1)
		{
			return 0;
		}
		
		if(result[input] > 0)
		{
			return result[input];
		}
		
		if(input % 3 == 0)
		{
			count3 = solve(input/3) + 1;
		}
		if(input % 2 == 0)
		{
			count2 = solve(input/2) + 1;
		}
		count1 = solve(input-1) + 1;
		
		result[input] = Math.min(Math.min(count3, count2), count1);
		return result[input];
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
