package dp.p1003C;

import java.util.Scanner;

public class Main
{
	private int zero;
	private int one;

	public Main()
	{
		Scanner scanner = new Scanner(System.in);
		
		int testCases = scanner.nextInt();
		String[] results = new String[testCases];
		for(int i = 0; i < testCases; i++)
		{
			int input = scanner.nextInt();
			results[i] = solve(input);
		}
		for (String string : results)
		{
			System.out.println(string);
		}
		scanner.close();
		return;
	}

	private String solve(int input)
	{
		zero = 0;
		one = 0;
		
		fibonacci(input);
		return zero + " " + one;
	}
	
	private int fibonacci(int n)
	{
		if (n == 0)
		{
			zero++;
			return 0;
		}
		else if (n == 1)
		{
			one++;
			return 1;
		}
		else
		{
			return fibonacci(n-1) + fibonacci(n-2);
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
