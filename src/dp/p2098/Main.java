package dp.p2098;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main
{
	public Main()
	{
		try
		{
			InputStreamReader is = new InputStreamReader(System.in);
			BufferedReader reader = new BufferedReader(is);

			int N;
			N = Integer.parseInt(reader.readLine());
			
			final int[][] map = new int[N][N];
			for(int i = 0; i < N; i++)
			{
				String[] temp = reader.readLine().split(" ");
				for(int j = 0; j < N; j++)
				{
					map[i][j] = Integer.parseInt(temp[j]);
				}
			}
			reader.close();
			is.close();
			
			List<int[]>[] k = new ArrayList[N]; 
			List<int[]> trace = new LinkedList<>();
			for(int i = 0; i < N; i++)
			{
				trace.clear();
				trace.add(new int[N]);
				int count = 0;
				trace.get(i)[count++] = i;
				do
				{
					for(int j = 0; j < N; j++)
					{
						if(map[i][j] > 0)
						{
							trace.get(i)[j] = 1;
						}
					}
				}
				while(check(trace.get(i)));
			}
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	private boolean check(int[] is)
	{
		// TODO Auto-generated method stub
		return false;
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
