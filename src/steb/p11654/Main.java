package steb.p11654;

import java.util.Scanner;

public class Main
{
	public Main()
	{
		Scanner scanner = new Scanner(System.in);

		String input = scanner.nextLine();
		
		run(input);
		scanner.close();
		return;
	}

	private void run(String input)
	{
		int a = (int) 'a';
		int z = (int) 'z';
		for(int i = a; i <= z; i++)
		{
			input.toCharArray();
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
