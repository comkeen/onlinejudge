package steb.p2675;

import java.util.Scanner;

public class Main
{
	public Main()
	{
		Scanner scanner = new Scanner(System.in);

		String test = scanner.nextLine();
		int testCase = Integer.parseInt(test);
		String[] cases = new String[testCase];
		for(int i = 0; i < testCase; i++)
		{
			cases[i] = scanner.nextLine();
		}
		run(cases);
		scanner.close();
		return;
	}

	private void run(String[] cases)
	{
		String[] results = new String[cases.length];

		
		for(int i = 0; i < cases.length; i++)
		{
			String result = "";
			
			String[] arr = cases[i].split(" ");
			
			int repeat = Integer.parseInt(arr[0]);
			String target = arr[1];
			
			char[] characters = target.toCharArray();
			for(int j = 0; j < characters.length; j++)
			{
				for(int k = 0; k < repeat; k++)
				{
					result += characters[j];
				}
			}
			results[i] = result;
			
		}
		for(int i = 0; i < results.length; i++)
		{
			System.out.println(results[i]);
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
