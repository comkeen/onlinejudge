package steb.p2292;

import java.util.Scanner;

public class Main
{
	private static final int RANGE = 1000000000;


	public Main()
	{
		Scanner scanner = new Scanner(System.in);

		int test = scanner.nextInt();
		for(int i = 0; i < test; i++)
		{
			String str = scanner.nextLine();
			String[] arr = str.split(" ");
			run(arr);
		}
		scanner.close();
		return;
	}

	private void run(String[] arr)
	{
		String result = "";
		
		int repeat = Integer.parseInt(arr[0]);
		String target = arr[1];
		char[] characters = target.toCharArray();
		for(int i = 0; i < characters.length; i++)
		{
			for(int j = 0; j < repeat; j++)
			{
				result += characters[i];
			}
		}
		System.out.println(result);
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
