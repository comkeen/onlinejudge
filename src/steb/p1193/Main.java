package steb.p1193;

import java.util.Scanner;

public class Main
{
	private static final int RANGE = 10000000;


	public Main()
	{
		Scanner scanner = new Scanner(System.in);

		int input = scanner.nextInt();
		if(1 <= input && input <= RANGE)
		{
			run(input);
		}
		scanner.close();
		return;
	}

	private void run(int input)
	{
		int parent = 0;
		int child = 0;
		int sum = 0;
		int pSum = 0;

		for(int i = 1; i <= RANGE; i++)
		{
			sum = pSum + i;
			if(input <= sum)
			{
				int index = input - pSum;
				if(i%2 == 0)
				{					
					parent = i - index + 1;
					child = index;
					String str = child + "/" + parent;
					System.out.println(str);
					return;
				}
				else
				{
					parent = index;
					child = i - index + 1;
					String str = child + "/" + parent;
					System.out.println(str);
					return;
				}
			}
			pSum = sum;
		}	
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
