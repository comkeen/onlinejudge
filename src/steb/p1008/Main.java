package steb.p1008;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main
{
	public Main()
	{
		Scanner scanner = new Scanner(System.in);

		int a = scanner.nextInt();
		int b = scanner.nextInt();
		double result = (double)a/(double)b;
		DecimalFormat df = new DecimalFormat("0.000000000#####");
		System.out.println(df.format(result));
		scanner.close();
		return;
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
