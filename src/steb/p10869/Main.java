package steb.p10869;

import java.util.Scanner;

public class Main
{
	public Main()
	{
		Scanner scanner = new Scanner(System.in);

		int a = scanner.nextInt();
		int b = scanner.nextInt();
		System.out.println(a+b);
		System.out.println(a-b);
		System.out.println(a*b);
		System.out.println(a/b);
		System.out.println(a%b);
		scanner.close();
		return;
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
