package greedy.p11047C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	private int N;
	private int K;

	public Main() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			String[] tokens = br.readLine().split(" ");
			N = Integer.parseInt(tokens[0]);
			K = Integer.parseInt(tokens[1]);
			final int[] inputs = new int[N];
			for(int i = 0; i < N; i++) {
				inputs[i] = Integer.parseInt(br.readLine());
			}
			br.close();
			isr.close();
			
			solve(inputs);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new Main();
	}

	private void solve(final int[] inputs) {
		
		int temp = K;
		int coinNumbers = 0;
		for(int i = N-1; i >= 0; i--) {
			int dib = temp/inputs[i];
			if(dib > 0) {
				coinNumbers += dib;
				temp = temp%inputs[i];
			}
		}
		System.out.println(coinNumbers);
	}
}
