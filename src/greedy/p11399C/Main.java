package greedy.p11399C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

	private int N;

	public Main() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			N = Integer.parseInt(br.readLine());
			final String[] tokens = br.readLine().split(" ");
			final int[] inputs = new int[N];
			for(int i = 0; i < N; i++) {
				inputs[i] = Integer.parseInt(tokens[i]);
			}
			br.close();
			isr.close();
			
			solve(inputs);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new Main();
	}

	private void solve(final int[] inputs) {
		Arrays.sort(inputs);
		int total = 0;
		int sum = 0;
		for(int i = 0; i < N; i++) {
			sum += inputs[i];
			total += sum;
		}
		System.out.println(total);
	}
}
