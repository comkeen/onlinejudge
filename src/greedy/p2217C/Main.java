package greedy.p2217C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

	private int N;

	public Main() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			N = Integer.parseInt(br.readLine());
			final int[] inputs = new int[N];
			for(int i = 0; i < N; i++) {
				inputs[i] = Integer.parseInt(br.readLine());
			}
			br.close();
			isr.close();
			
			solve(inputs);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new Main();
	}

	private void solve(final int[] inputs) {
		Arrays.sort(inputs);
		int max = 0;
		for(int i = 0; i < N; i++) {
			int LimitedWeight = inputs[i]*(N-i); 
			if(max < LimitedWeight) {
				max = LimitedWeight;
			}
		}
		System.out.println(max);
	}
}
