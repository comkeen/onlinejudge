package bfs.p5014C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class Main
{
	public static final String IMPOSSIBLE = "use the stairs";

	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String[] inputs = reader.readLine().split(" ");
			int F = Integer.parseInt(inputs[0]);
			int S = Integer.parseInt(inputs[1]);
			int G = Integer.parseInt(inputs[2]);
			int U = Integer.parseInt(inputs[3]);
			int D = Integer.parseInt(inputs[4]);
			reader.close();
			solve(F, S, G, U, D);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void solve(int F, int S, int G, int U, int D)
	{
		int[] nextStep = {U, -D};
		boolean[] visit = new boolean[F+1];
		visit[S] = true;
		Queue<Node> queue = new LinkedList<>();

		Node node = new Node(S, 0);
		queue.offer(node);

		while(!queue.isEmpty())
		{
			node = queue.poll();
			
			if(node.pos == G)
			{
				System.out.println(node.distance);
				return;
			}
			
			for(int i = 0; i < 2; i++)
			{
				int	next = node.pos + nextStep[i];
				if(next >= 1 && next <= F)
				{
					if(!visit[next])
					{
						visit[next] = true;
						queue.offer(new Node(next, node.distance + 1));
					}
				}
			}
		}
		System.out.println(IMPOSSIBLE);
	}

	public static void main(String[] args)
	{
		new Main();
	}

	public class Node
	{
		public int pos;
		public int distance;

		public Node(int pos, int distance)
		{
			this.pos = pos;
			this.distance = distance;
		}
	}
}
