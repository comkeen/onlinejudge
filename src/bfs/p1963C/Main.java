package bfs.p1963C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class Main 
{
	public static final String IMP = "Impossible";
	private Map<String, String> primes;
	private Set<String> visited;

	public Main()
	{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		String[] inputs = null;
		int N = 0;
		try
		{
			String top = br.readLine();
			N = Integer.parseInt(top);

			inputs = new String[N];
			for(int i = 0; i < N; i++)
			{
				inputs[i] = br.readLine();
			}
			br.close();
			isr.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		primes = primeMemoization();

		int[] results = new int[N];
		for(int i = 0; i < N; i++)
		{
			String[] temps = inputs[i].split(" ");
			results[i] = solve(temps[0], temps[1]);
		}

		for(int i = 0; i < N; i++)
		{
			if(results[i] != -1)
			{
				System.out.println(results[i]);
			}
			else
			{
				System.out.println(IMP);
			}
		}
	}

	private Map<String, String> primeMemoization()
	{
		Map<String, String> map = new HashMap<>();
		find:for(int i = 1000; i <= 9999; i++)
		{
			for(int j = 2; j <= i/2; j++)
			{
				if(i%j == 0)
				{
					continue find;
				}
			}
			String primeNum = Integer.toString(i);
			map.put(primeNum, primeNum);
		}
		return map;
	}

	private int solve(String start, String goal)
	{
		visited = new HashSet<>();
		visited.add(start);
		return bfs(start, goal);
	}

	private void solve_r(String start, String goal)
	{
		visited = new HashSet<>();
		visited.add(start);
		System.out.println(bfs_r(start, goal));
	}

	private int bfs(final String start, final String goal)
	{
		final Queue<Prime> queue = new LinkedList<>();

		queue.offer(new Prime(start, 0));		

		while(!queue.isEmpty())
		{
			final Prime prime = queue.poll();

			String temp = prime.str;
			visited.add(temp);
			if(goal.equals(temp))
			{
				return prime.distance;
			}
			for(int i = 0; i < 4; i++)
			{
				final char s = temp.charAt(i);
				final char g = goal.charAt(i);
				if(Character.compare(s, g) != 0)
				{
					for(int j = 0; j < 10; j++)
					{
						char[] cTemp = temp.toCharArray();
						cTemp[i] = Integer.toString(j).charAt(0);
						String str = String.valueOf(cTemp);
						if(primes.containsKey(str) && !visited.contains(str))
						{
							queue.offer(new Prime(str, prime.distance + 1));
						}
					}
				}
			}
		}
		return -1;
	}

	private int bfs_r(final String temp, final String goal)
	{
		visited.add(temp);
		if(goal.equals(temp))
		{
			return 0;
		}
		for(int i = 0; i < 4; i++)
		{
			char s = temp.charAt(i);
			char g = goal.charAt(i);
			if(Character.compare(s, g) != 0)
			{
				for(int j = 0; j < 10; j++)
				{
					char[] cTemp = temp.toCharArray();
					cTemp[i] = Integer.toString(j).charAt(0);
					String str = String.valueOf(cTemp);
					if(primes.containsKey(str) && !visited.contains(str))
					{
						return bfs_r(str, goal) + 1;
					}
				}
			}
		}
		return 0;
	}

	public static void main(String[] args)
	{
		new Main();
	}

	public class Prime
	{
		public String str;
		public int distance;

		public Prime(String str, int distance)
		{
			this.str = str;
			this.distance = distance;
		}
	}
}
