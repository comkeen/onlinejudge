package bfs.p2178C;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main 
{    
	public static int MAX_SIZE = 128;
	private int[][] map;
	private int n, m;
	private boolean[][] visited;
	private int[][] result;
	
	public Main()
	{
		Scanner scanner = new Scanner(System.in);

		String mapSize = scanner.nextLine();
		String[] mapToken = mapSize.split(" ");
		n = Integer.parseInt(mapToken[0]);
		m = Integer.parseInt(mapToken[1]);
		map = new int[n][m];
		
		for (int i = 0; i < n; i++)
	    {
            char[] temp = scanner.nextLine().toCharArray();
            for (int j = 0; j < m; j++)
            {
	    		map[i][j] = temp[j] - '0';
	    	}
	    }
		bfs(0, 0);
		
		scanner.close();
	}
	
	private void bfs (int xPos, int yPos)
	{
		
		Queue<Point> queue = new LinkedList<Point>();
		visited = new boolean[n][m];
		result = new int[n][m];
		
		int[] dx = {1, -1, 0, 0};
		int[] dy = {0, 0, 1, -1};
		
		queue.offer(new Point(0, 0));

		
	    while(!queue.isEmpty()) {
	    	Point temp = queue.poll();
	    	int tempX = temp.xPos;
	    	int tempY = temp.yPos;
	    	
	    		for (int i = 0; i < 4; ++i) {
	    			int nx = tempX + dx[i];
	    			int ny = tempY + dy[i];
	    			
	    			if (nx >= 0 && ny >= 0 && nx < n && ny < m && !visited[nx][ny] && map[nx][ny] == 1) {
	    				queue.offer(new Point(nx, ny));
	    				visited[nx][ny] = true;
	    				result[nx][ny] = result[tempX][tempY] + 1;
	    			}
	    		}
	    }
	    System.out.println(result[n-1][m-1] + 1);
	}

	public static void main(String[] args)
	{
		new Main();
	}
	
	public class Point
	{
		int xPos;
		int yPos;
		
		public Point(int xPos, int yPos)
		{
			this.xPos = xPos;
			this.yPos = yPos;
		}
	}
}