package bfs.test;

import org.junit.Test;


public class TestRecursive
{
	public static final int INPUT = 10000;
//	public static final int INPUT = 1000;
	
	@Test
	public void testRecursive()
	{
		final int result = addOne(INPUT);
		System.out.println(result);
	}

	@Test
	public void testTailRecursive()
	{
		addOne2(INPUT);
	}
	
	private int addOne(int n)
	{
		if(n == 1)
		{
			return 1;
		}
		return addOne(n-1) + 1;
	}
	
	private void addOne2(int n)
	{
		if(n == 1)
		{
			System.out.println(n);
		}
		int next = n-1;
		addOne2(next);
	}
}
