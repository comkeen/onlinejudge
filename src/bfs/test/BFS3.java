package bfs.test;

import java.util.*;



public class BFS3 
{    
	private int[][] map;
	private int n, m;

	public BFS3()
	{
		Scanner scanner = new Scanner(System.in);

		String mapSize = scanner.nextLine();
		String[] mapToken = mapSize.split(" ");
		n = Integer.parseInt(mapToken[0]);
		m = Integer.parseInt(mapToken[1]);

		map = new int[n][m];
	    for (int i = 0; i < n; i++)
	    {
            String temp = scanner.nextLine();
            char[] temps = temp.toCharArray();
	    	for (int j = 0; j < m; j++)
	    	{
	            int value = Integer.parseInt(Character.toString(temps[j]));
	    		map[i][j] = value;
	    	}
	    }
	    int result = bfs(0, 0);
		System.out.println(result);
		scanner.close();
		return;
	}
	
	private int bfs(int xPos, int yPos)
	{
		int min = n*m;
		Queue<Position> q = new LinkedList<Position>();
		q.offer(new Position(xPos, yPos, 1));
		
	    while(!q.isEmpty())
	    {	        
	    	Position temp = q.poll();
	        map[temp.yPos][temp.xPos] = 0;

	        
	        //up
	        if (temp.yPos > 0 && map[temp.yPos - 1][temp.xPos] == 1)
	        {
	        	q.offer(new Position(temp.xPos, temp.yPos-1, temp.l + 1));
	        }
	        //down
	        if (temp.yPos < n - 1 && map[temp.yPos + 1][temp.xPos] == 1)
	        {
	        	q.offer(new Position(temp.xPos, temp.yPos+1, temp.l + 1));
	        }
	        //left
	        if (temp.xPos > 0 && map[temp.yPos][temp.xPos - 1] == 1)
	        {
	        	q.offer(new Position(temp.xPos-1, temp.yPos, temp.l + 1));
	        }
	        //right
	        if (temp.xPos < m - 1 && map[temp.yPos][temp.xPos + 1] == 1)
	        {
	        	q.offer(new Position(temp.xPos+1, temp.yPos, temp.l + 1));
	        }
        	min = temp.l;
	    }
	    return min;
	}

	public static void main(String[] args)
	{
		new BFS3();        
	}

	public class Position
	{
		public int xPos, yPos, l;
		
		public Position(int x, int y, int l)
		{
			this.xPos = x;
			this.yPos = y;;
			this.l = l;
		}
		
		public void print()
		{
			System.out.println("x.y,l:" + this.xPos + "." + this.yPos + "," + this.l);
		}
	}
}