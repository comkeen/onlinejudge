package bfs.test;

import java.util.Scanner;

public class BFS2
{
	private Scanner scanner;
	
	private int mapSize, count;
	
	private int[][] map;
	private int[] x, y, l;
	
	private void init()
	{
		map = new int[16][16];
		x = new int[32];
		y = new int[32];
		l = new int[32];
	}
	
	public BFS2()
	{
		scanner = new Scanner(System.in);

		init();
		mapSize = scanner.nextInt();		
	    for (int i = 0; i < mapSize; i++)
	    {
	    	for (int j = 0; j < mapSize; j++)
	    	{
	            map[i][j] = scanner.nextInt();
	    	}
	    }
	    BFS(0, 0);
		
		scanner.close();
		return;
	}

	
	// 큐에 좌표 정보와 길이를 삽입하는 함수
	private void enqueue(int xPos, int yPos, int length)
	{
	    x[count] = xPos;
	    y[count] = yPos;
	    l[count] = length;
	    count++;
	}
	
	private void BFS(int xPos, int yPos)
	{		
		int pos = 0;
		 
	    // 시작점의 좌표 정보와 길이를 큐에 삽입한다
	    enqueue(xPos, yPos, 1);
	    
	    // 더 이상 방문할 지점이 없거나, 도착 지점에 도착하면 루프를 탈출한다
	    while (pos < count && (x[pos] != mapSize - 1 || y[pos] != mapSize - 1))
	    {
	        // 두 번 방문하게 하면 안되므로, 이미 지나갔다는 표시를 해둔다
	        map[y[pos]][x[pos]] = 0;
	 
	        // 위로 갈 수 있다면, 위 지점의 좌표 정보와 길이를 큐에 삽입한다
	        if (y[pos] > 0 && map[y[pos] - 1][x[pos]] == 1)
	        {
	            enqueue(x[pos], y[pos] - 1, l[pos] + 1);	        	
	        }
	        // 아래로 갈 수 있다면, 아래 지점의 좌표 정보와 길이를 큐에 삽입한다
	        if (y[pos] < mapSize - 1 && map[y[pos] + 1][x[pos]] == 1)
	        {
	            enqueue(x[pos], y[pos] + 1, l[pos] + 1);	        	
	        }
	        // 왼쪽으로 갈 수 있다면, 왼쪽 지점의 좌표 정보와 길이를 큐에 삽입한다
	        if (x[pos] > 0 && map[y[pos]][x[pos] - 1] == 1)
	        {
	            enqueue(x[pos] - 1, y[pos], l[pos] + 1);	        	
	        }
	        // 오른쪽로 갈 수 있다면, 오른쪽 지점의 좌표 정보와 길이를 큐에 삽입한다
	        if (x[pos] < mapSize - 1 && map[y[pos]][x[pos] + 1] == 1)
	        {
	            enqueue(x[pos] + 1, y[pos], l[pos] + 1);
	        }
	 
	        // 큐의 다음 순서의 지점을 방문한다
	        pos++;
	    }
	 
	    // cnt가 pos보다 크다면, 도착 지점에 무사히 도착한 것이라고 말할 수 있다.
	    // 위의 반복문은 도착점에 도착하게 되면 루프를 바로 빠져나오기 때문에,
	    // 길이를 삽입하는 큐의 마지막 요소가 최단 경로의 길이라고 할 수 있다.
	    if (pos < count)
	    {
	    	System.out.println("최단 경로 길이: " + l[pos]);
	    	System.out.println("xArr");
	    	printArray(x);
	    	System.out.println("yArr");
	    	printArray(y);
	    	System.out.println("lArr");
	    	printArray(l);
	    }
	}
	
	private void printArray(int[] input)
	{
		for(int i = 0; i < input.length; i++)
		{
			System.out.printf("%2d ", input[i]);
		}
		System.out.println();
	}
	
	public static void main(String[] args)
	{
		new BFS2();
	}
}
