package bfs.test;

import java.util.Scanner;

public class BFS1
{
	private Scanner scanner;
	
	private int max;
	private int front, back;
	int[][] map;
	int[] queue, visit;

	public BFS1()
	{
		scanner = new Scanner(System.in);

		init();
		
		scanner.close();
		return;
	}

	private void init()
	{
		visit = new int[32];
		queue = new int[32];
		map = new int[32][32];
		
		solve();
	}
	
	private void solve()
	{
		int v1, v2;

		max = scanner.nextInt();
		int start = scanner.nextInt();
		
		while(true)
		{
			v1 = scanner.nextInt();
			v2 = scanner.nextInt();
			if(v1 == -1 && v2 == -1)
			{
				break;
			}
			map[v1][v2] = 1;
			map[v2][v1] = 1;
		}
		BFS(start);
	}
	
	private void BFS(int v)
	{		
		visit[v] = 1; // 정점 v를 방문했다고 표시		
		System.out.println(v + "에서 시작!");
		
		
		queue[back++] = v; // 큐에 v를 삽입하고 후단을 1 증가시킴
		System.out.println("back,queue:" + back + "," + queue[back]);
		while(front < back) // 후단이 전단과 같거나 작으면 루프 탈출
		{
			// 큐의 첫번째에 있는 데이터를 제외하고 제외된 값을 가져오며, 전단 1 증가
			v = queue[front++]; 
			System.out.println("front,queue:" + front + "," + queue[front]);
			for(int i = 1; i <= max; i++)
			{
				// 정점 v와 정점 i가 만나고, 정점 i를 방문하지 않은 상태일 경우
				if(map[v][i] == 1 && visit[i] == 0)
				{
					visit[i] = 1; // 정점 i를 방문했다고 표시
					System.out.println(v + "에서 " + i + "로 이동");
					
					queue[back++] = i; // 큐에 i를 삽입하고 후단을 1 증가시킴
				}
			}
		}
		
	}

	public static void main(String[] args)
	{
		new BFS1();
	}
}
