package bfs.p1697C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String[] inputs = reader.readLine().split(" ");
			int init = Integer.parseInt(inputs[0]);
			int goal = Integer.parseInt(inputs[1]);
			reader.close();
			
			solve(init, goal);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(int init, int goal)
	{
		int[] nextStep = {1, -1, 2};
		boolean[] visit = new boolean[100001];
		Arrays.fill(visit, false);
		Queue<Node> queue = new LinkedList<>();
		
		Node node = new Node(init, 0);
		queue.offer(node);
		
		while(!queue.isEmpty())
		{
			node = queue.poll();
			if(node.pos == goal)
			{
				break;
			}
			for(int i = 0; i < 3; i++)
			{
				int next = 0;
				if(i == 2)
				{
					next = node.pos*nextStep[i];
				}
				else
				{
					next = node.pos + nextStep[i]; 
				}
				if(next >= 0 && next <= 100000 && !visit[next])
				{
					visit[node.pos] = true;
					queue.offer(new Node(next, node.distance + 1));
				}
			}
		}
		System.out.println(node.distance);
	}
	
	public static void main(String[] args)
	{
		new Main();
	}
	
	public class Node
	{
		public int pos;
		public int distance;
		
		public Node(int pos, int distance)
		{
			this.pos = pos;
			this.distance = distance;
		}
	}
}
