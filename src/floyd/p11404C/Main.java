package floyd.p11404C;

import java.util.Scanner;

public class Main
{
	private static final int INFINITE = 100001;

	private Scanner scanner;

	int[][] distance;

	public Main()
	{
		scanner = new Scanner(System.in);

		int n = scanner.nextInt();
		int cases = scanner.nextInt();
		distance = new int[n][n];
		distance = setInfinite(distance);
		
		for(int i = 0; i < cases; i++)
		{
			int a = scanner.nextInt() - 1;
			int b = scanner.nextInt() - 1;
			int cost = scanner.nextInt();
			if(cost < distance[a][b])
			{
				distance[a][b] = cost;
			}
		}
		scanner.close();

		solve(distance);
		return;
	}

	private void print(int[][] arr)
	{
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < arr.length; i++)
		{
			for(int j = 0; j < arr[0].length; j++)
			{
				sb.append(arr[i][j]);
				if(j != arr[0].length-1)
				{
					sb.append(" ");
				}
			}
			sb.append("\n");
		}
		System.out.print(sb.toString());
	}

	private int[][] setInfinite(int[][] arr)
	{
		for(int i = 0; i < arr.length; i++)
		{
			for(int j = 0; j < arr[0].length; j++)
			{
				arr[i][j] = INFINITE;
			}
		}
		return arr;
	}
	
	private int[][] check(int[][] arr)
	{
		for(int i = 0; i < arr.length; i++)
		{
			for(int j = 0; j < arr[0].length; j++)
			{
				if(i == j || arr[i][j] == INFINITE)
				{
					arr[i][j] = 0;					
				}
			}
		}
		return arr;
	}
	
	private void solve(int[][] arr)
	{
		arr = Floyd(arr);
		arr = check(arr);
		print(arr);
	}

	private int[][] Floyd(int[][] arr)
	{
		for (int k = 0; k < arr.length; k++)
		{
			for(int i = 0; i < arr.length; i++)
			{
				for(int j = 0; j < arr.length; j++)
				{
					if(arr[i][j] > arr[i][k]+arr[k][j])
					{
						arr[i][j] = arr[i][k]+arr[k][j];
					}
				}
			}
		}
		return arr;
	}


	public static void main(String[] args)
	{
		new Main();
	}
}
