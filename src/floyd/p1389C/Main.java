package floyd.p1389C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public static final int INF = 10000;
	int n, m;
	int[][] adjacent;

	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			String[] token = top.split(" ");
			n = Integer.parseInt(token[0]);
			m = Integer.parseInt(token[1]);

			adjacent = new int[n][n];
			for(int i = 0; i < n; i++)
			{
				for(int j = 0; j < n; j++)
				{
					if(i == j)
					{
						adjacent[i][j] = 0;
					}
					else
					{
						adjacent[i][j] = INF;
					}
				}
			}
			for(int i = 0; i < m; i++)
			{
				String[] temps = reader.readLine().split(" ");
				int from = Integer.parseInt(temps[0]) - 1;
				int to = Integer.parseInt(temps[1]) - 1;
				adjacent[to][from] = adjacent[from][to] = 1;
			}
			reader.close();

			floyd();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void floyd()
	{
		for(int k = 0; k < n; k++)
		{
			for(int i = 0; i < n; i++)
			{
				for(int j = 0; j < n; j++)
				{
					if(adjacent[i][j] > adjacent[i][k] + adjacent[k][j])
					{
						adjacent[i][j] = adjacent[i][k] + adjacent[k][j];
					}
				}
			}
		}

		int min = INF;
		int index = 0;
		for(int i = 0; i < n; i++)
		{
			int sum = 0;
			for(int j = 0; j < n; j++)
			{
				if(adjacent[i][j] >= INF)
				{
					adjacent[i][j] = 0;
				}
				sum += adjacent[i][j];
			}
			if(min > sum)
			{
				min = sum;
				index = i;
			}
		}

		System.out.println(index+1);
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
