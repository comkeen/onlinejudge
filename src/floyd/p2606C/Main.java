package floyd.p2606C;

import java.util.Scanner;

public class Main
{
	private Scanner scanner;
	
	int[][] adMatrix;
	
	public Main()
	{
		scanner = new Scanner(System.in);

		int n = scanner.nextInt();
		int cases = scanner.nextInt();
		adMatrix = new int[n][n];
		
		for(int i = 0; i < cases; i++)
		{
			int a = scanner.nextInt() - 1;
			int b = scanner.nextInt() - 1;
			adMatrix[a][b] = adMatrix[b][a] = 1;
		}
		scanner.close();
		
		solve(n);
		return;
	}

	private void solve(int n)
	{
		int[] visit = new int[n];
		DFS(0, visit);
		int result = countComputer(visit);
		System.out.println(result);
	}
	private void DFS(int i, int[] visit)
	{		
		visit[i] = 1;
	    for (int j = 0; j < adMatrix[0].length; j++)
	    {
	        if (adMatrix[i][j] == 1 && visit[j] == 0)
	        {
	            DFS(j, visit);
	        }
	    }
	}
	
	private int countComputer(int[] arr)
	{
		int count = 0;
		for(int i = 0; i < arr.length; i++)
		{
			if(arr[i] == 1)
			{
				count++;
			}
		}
		return count - 1;
	}
	
	public static void main(String[] args)
	{
		new Main();
	}
}
