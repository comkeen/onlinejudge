package dikstra.p1753C;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;


public class Accept
{
	int INF = 987654321;
	int[] dist;  //start node���� i node������ �����ִܰŸ�
	boolean[] visited ;// �� ��庰�� �湮�� ���� �ִ��� ǥ��
	int[] parent;        // ������� ���� ����ȣ
	int N ,M,S,E;
	PriorityQueue<Element> pq = new  PriorityQueue<Element>();
	int[][] edge;


	public class Element implements Comparable<Element>{
		private int Vertex;
		private int distance;

		public Element(int Vertex, int distance){
			this.Vertex = Vertex;
			this.distance = distance;
		}
		public int compareTo(Element o){
			return distance - o.distance;
		}
	}

	public Accept()
	{
		Scanner sc = new Scanner(System.in);
		N=sc.nextInt(); M=sc.nextInt(); S=sc.nextInt();
		dist = new int[N+1];
		edge=new int[M+1][3];
		visited= new boolean[N+1];
		parent = new int[N+1];
		for(int i=0;i<M;i++){
			int A=sc.nextInt(); int B=sc.nextInt();int C=sc.nextInt();
			edge[i][0]=A;
			edge[i][1]=B;
			edge[i][2]=C;
		}
		Arrays.fill(dist, INF);
		Arrays.fill(visited, false);
		Arrays.fill(parent, S);//���� node start ���� �ʱ�ȭ

		dist[S] = 0;

		shortestPath(S);

		for(int i=1;i<=N;i++){
			if(dist[i]==INF)System.out.println("INF");
			else System.out.println(dist[i]);
		}
		sc.close();
	}

	public void shortestPath(int start)
	{
		pq.add(new Element(start, dist[start]));

		while(!pq.isEmpty())
		{
			int distance = pq.peek().distance;
			int here = pq.peek().Vertex;
			pq.poll();
			visited[here]=true;

			if(distance > dist[here]) //���� ����� �̹� ������Ʈ �Ȱ�
			{
				continue;
			}
			for(int i = 0; i < M; i++) //��������Ʈ�� ���鼭 �ּҰ�� �����ϰ�
			{
				int from=edge[i][0];
				int to=edge[i][1];
				int cost=edge[i][2];
				if(visited[to]==false&&from==here && dist[to] > dist[here] + cost)
				{
					dist[to] = dist[here] + cost;
					parent[to] = here;    // �ּҰ�θ� �����ϴ� �����
					pq.add(new Element(to, dist[to])); //�湮�� ����Ʈ�� ��带 pq�� �־���
				}
			}
		}
	}

	public static void main(String[] args) throws Exception
	{
		new Accept();
	}
}