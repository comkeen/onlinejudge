package dikstra.p1238C;

import java.util.Scanner;

public class Floyd
{
	private static final int INFINITE = 1000000000;

	private int n, m, x;
	private int[][] adj;
	
	public Floyd()
	{
		Scanner scanner = new Scanner(System.in);

		n = scanner.nextInt();
		m = scanner.nextInt();
		x = scanner.nextInt() - 1;
		
		adj = new int[n][n];
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
			{
				adj[i][j] = INFINITE;			
			}
		}
		
		for(int i = 0; i < m; i++)			
		{
			int start = scanner.nextInt() - 1;
			int end = scanner.nextInt() - 1;
			int time = scanner.nextInt();
			if(time < adj[start][end])
			{
				adj[start][end] = time;
			}
		}		
		scanner.close();
		
		FW();
		caculate();
		return;
	}

	private void FW()
	{
		for (int k = 0; k < n; k++)
		{
			for(int i = 0; i < n; i++)
			{
				for(int j = 0; j < n; j++)
				{
					if(adj[i][j] > adj[i][k]+adj[k][j])
					{
						adj[i][j] = adj[i][k]+adj[k][j];
					}
				}
			}
		}
	}
	
	private void caculate()
	{
		int max = 0;
		for(int i = 0; i < adj.length; i++)
		{
			int temp = adj[i][x] + adj[x][i];
			if(max < temp)
			{
				max = temp;
			}
		}
		System.out.println(max);
	}

	public static void main(String[] args)
	{
		new Floyd();
	}
}
