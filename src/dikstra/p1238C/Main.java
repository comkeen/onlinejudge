package dikstra.p1238C;

import java.util.Scanner;

public class Main
{
	private static final int INFINITE = 1000000000;

	private Scanner scanner;
	
	int[][] adj, dikstraResult;
	private int n, m, x;

	public Main()
	{
		scanner = new Scanner(System.in);

		n = scanner.nextInt();
		m = scanner.nextInt();
		x = scanner.nextInt() - 1;
		
		adj = new int[n][n];
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
			{
				adj[i][j] = INFINITE;
			}
		}
		
		for(int i = 0; i < m; i++)
		{
			int start = scanner.nextInt() - 1;
			int end = scanner.nextInt() - 1;
			int time = scanner.nextInt();
			if(time < adj[start][end])
			{
				adj[start][end] = time;
			}
		}
		scanner.close();
		
		dikstraResult = new int[n][n];
		for(int i = 0; i < n; i++)
		{
			dikstraResult[i] = dikstra(i, x);
		}

		int max = 0;
		for(int i = 0; i < n; i++)
		{
			if(i == x)
			{
				continue;
			}
			int temp = dikstraResult[i][x] + dikstraResult[x][i];
			if(max < temp)
			{
				max = temp;
			}
		}
		System.out.println(max);
		
		return;
	}
	
	private int[] dikstra(int start, int end)
	{
        boolean[] isVisits = new boolean[n];
        int[] distance = new int[n];
        
        int nextVertex = start;
        int min = 0;
        
        for (int i = 0; i < n; i++)
        {
            isVisits[i] = false;
            distance[i] = INFINITE;
        }
        distance[start] = 0;
 
        while (true)
        {
            min = INFINITE;
            for (int j = 0; j < n; j++)
            {
            	
                if (!isVisits[j] && distance[j] < min)
                {
                    nextVertex = j;
                    min = distance[j];
                }
            }
            if (min == INFINITE)
            {
            	break;
            }
            isVisits[nextVertex] = true;
 
            for (int j = 0; j < n; j++)
            {
                int distanceVertex = distance[nextVertex] + adj[nextVertex][j];
                if (distance[j] > distanceVertex)
                {
                    distance[j] = distanceVertex;
                }
            }
            
        }
        
        return distance;
    }
	
	public static void main(String[] args)
	{
		new Main();
	}
}
