package math.p1904C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public static final int MOD = 15746;
	
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			int n = Integer.parseInt(top);
			reader.close();
			
			solve(n);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(int range)
	{
		int pre = 1;
		int next = 2;
		int temp;
		for(int i = 3; i <= range; i++)
		{
			temp = next;
			next = next%MOD + pre%MOD;
			pre = temp;
		}
		
		System.out.println(next%MOD);
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
