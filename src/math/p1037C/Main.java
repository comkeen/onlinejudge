package math.p1037C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {
	
	public Main() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader reader = new BufferedReader(isr);

			int T = Integer.parseInt(reader.readLine());
			String[] tokens = reader.readLine().split(" ");
			int[] inputs = new int[T];
			for(int i = 0; i < T; i++)
			{
				inputs[i] = Integer.parseInt(tokens[i]);
			}
			reader.close();
			isr.close();
			
			solve(inputs);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new Main();
	}
	
	private void solve(int[] inputs) {
		Arrays.sort(inputs);
		int min = inputs[0];
		int max = inputs[inputs.length-1];
		
		System.out.println(min*max);
	}
}
