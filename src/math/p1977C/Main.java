package math.p1977C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public static final int INF = Integer.MAX_VALUE/2;
	
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String temp = reader.readLine();
			int m = Integer.parseInt(temp);
			temp = reader.readLine();
			int n = Integer.parseInt(temp);
			reader.close();
			
			solve(m, n);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(int m, int n)
	{
		int sum = 0;
		int temp = 1;
		int square = 0;
		int min = INF;
		
		while(true)
		{
			square = temp*temp;
			if(square > n)
			{
				break;
			}
			if(square >= m)
			{
				sum += square;
				if(min > square)
				{
					min = square;
				}
			}
			temp++;
		}
		if(min != INF)
		{
			System.out.println(sum);
			System.out.println(min);
		}
		else
		{
			System.out.println(-1);
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
