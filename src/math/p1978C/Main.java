package math.p1978C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			int cases = Integer.parseInt(top);

			int[] inputs = new int[cases];
			String[] targets = reader.readLine().split(" ");
			for(int i = 0; i < cases; i++)
			{
				inputs[i] = Integer.parseInt(targets[i]);
			}
			solve(inputs);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void solve(int[] input)
	{
		int count = 0;
		for(int i = 0; i < input.length; i++)
		{
			int target = input[i];
			if(target == 1)
			{
				continue;
			}
			for(int j = 2; j <= target; j++)
			{
				if(target/j < 2)
				{
					count++;
					break;
				}
				if(target%j == 0)
				{
					break;
				}
			}
		}
		System.out.println(count);
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
