package math.p1010C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			int cases = Integer.parseInt(top);

			String[] inputs = new String[cases];
			for(int i = 0; i < cases; i++)
			{
				inputs[i] = reader.readLine();
			}
			reader.close();

			int[][] result = solve_f(30);
			for(int i = 0; i < inputs.length; i++)
			{
				String[] temps = inputs[i].split(" ");
				int w = Integer.parseInt(temps[0]);
				int e = Integer.parseInt(temps[1]);
				System.out.println(result[e][w]);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private int solve_r(int w, int e)
	{
		int result = 0;
		if(w == 1)
		{
			return e;
		}
		else
		{
			for(int i = 1; i < e; i++)
			{
				result += solve_r(w-1, e - i);
			}
		}
		return result;
	}

	private int[][] solve_f(int e)
	{
		int[][] dp = new int[e][e];
		for(int i = 0; i < e; i++)
		{	
			dp[i][0] = 1;
		}
		for(int j = 0; j < e; j++)
		{
			dp[j][j] = 1;
		}
		for(int i = 1; i < e; i++)
		{
			for(int j = 1; j < e; j++)
			{
				dp[i][j] = dp[i-1][j-1] + dp[i-1][j];
			}
		}
		return dp;
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
