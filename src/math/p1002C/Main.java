package math.p1002C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			int cases = Integer.parseInt(top);

			String[] inputs = new String[cases];
			for(int i = 0; i < cases; i++)
			{
				inputs[i] = reader.readLine();
			}
			reader.close();
			
			for(int i = 0; i < inputs.length; i++)
			{
				String[] temps = inputs[i].split(" ");
				int x1 = Integer.parseInt(temps[0]);
				int y1 = Integer.parseInt(temps[1]);
				int r1 = Integer.parseInt(temps[2]);		
				int x2 = Integer.parseInt(temps[3]);
				int y2 = Integer.parseInt(temps[4]);
				int r2 = Integer.parseInt(temps[5]);
				solve(x1, y1, r1, x2, y2, r2);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return;
	}

	private void solve(int x1, int y1, int r1, int x2, int y2, int r2)
	{
		int xDistance = x2 - x1;
		int yDistance = y2 - y1;
		int sqDistance = xDistance*xDistance + yDistance*yDistance;
		
		if(sqDistance == 0)
		{
			if(r2 == r1)
			{
				System.out.println(-1);
				return;
			}
			else
			{
				System.out.println(0);
				return;
			}
		}
		else
		{
			if((r2+r1)*(r2+r1) < sqDistance || (r2-r1)*(r2-r1) > sqDistance )
			{
				System.out.println(0);
				return;
			}
			else if((r2+r1)*(r2+r1) == sqDistance || (r2-r1)*(r2-r1) == sqDistance)
			{
				System.out.println(1);
				return;
			}
			else
			{
				System.out.println(2);
				return;
			}
		}
		
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
