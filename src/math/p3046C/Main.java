package math.p3046C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			int cases = Integer.parseInt(reader.readLine());
			int[] inputs = new int[cases];
			for(int i = 0; i < cases; i++)
			{
				inputs[i] = Integer.parseInt(reader.readLine());
			}

			solve(inputs);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(int[] inputs)
	{
		int result = 1;
		for(int i = 0; i < inputs.length; i++)
		{
			result = result + inputs[i] - 1;
		}
		System.out.println(result);
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
