package math.p1357C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			String[] token = top.split(" ");
			reader.close();
			solve(Integer.parseInt(token[0]), Integer.parseInt(token[1]));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(int left, int right)
	{
		int result = reverse(reverse(left) + reverse(right));
		System.out.println(result);
	}
	
	private int reverse(int target)
	{
		char[] t = Integer.toString(target).toCharArray();
		StringBuffer sb = new StringBuffer();
		for(int i = t.length-1; i >= 0; i--)
		{
			sb.append(t[i]);
		}
		return Integer.parseInt(sb.toString());
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
