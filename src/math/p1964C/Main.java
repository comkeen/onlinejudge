package math.p1964C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			int step = Integer.parseInt(top);

			solve(step);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void solve(int step)
	{
		int sum = 5;
		int dp = 4;
		if(step == 1)
		{
			System.out.println(sum);
			return;
		}
		for(int i = 2; i <= step; i++)
		{
			dp = (dp + 3)%45678;
			sum = (sum + dp)%45678;
		}
		System.out.println(sum);
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
