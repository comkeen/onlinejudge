package math.p1110C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			int input = Integer.parseInt(top);

			solve(input);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(int input)
	{
		int count = 0;
		int temp = input;
		int result = -1;
		
		while(input != result)
		{
			count++;
			int ten = temp/10;
			int one = temp%10;
			result = one*10 + (ten + one)%10;
			temp = result;
		}
		System.out.println(count);
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
