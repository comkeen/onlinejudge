package math.p1912C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {

	public static final String SCAN = "Scan";
	public static final String BUFF = "Buff";
	public static final String FAST = "Fast";

	public Main() {
		switch (BUFF) {
		case SCAN:
			sc();
			return;
		case FAST:
			fast();
			return;
		default:
			br();
			return;
		}
	}

	private void sc() {
		Scanner scanner = new Scanner(System.in);

		final int n = scanner.nextInt();
		final int[] inputs = new int[n];
		for(int i = 0; i < n; i++) {
			inputs[i] = scanner.nextInt();
		}
		scanner.close();

		System.out.println(maxSerializedSum(inputs));
	}

	private void br() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			final int n = Integer.parseInt(br.readLine());
			final String[] tokens = br.readLine().split(" ");
			final int[] inputs = new int[n];
			for(int i = 0; i < n; i++) {
				inputs[i] = Integer.parseInt(tokens[i]);
			}
			br.close();
			isr.close();

			System.out.println(maxSerializedSum(inputs));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void fast() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			final int n = Integer.parseInt(br.readLine());
			final String[] tokens = br.readLine().split(" ");

			int sum = 0;
			int nMax = -1001;
			int max = 0;
			for(int i = 0; i < n; i++) {
				int temp = Integer.parseInt(tokens[i]);
				if(sum + temp > 0) {
					sum += temp;
				} else {
					if(temp <= 0 && nMax < temp)	{
						nMax = temp;
					}
					sum = 0;
				}
				if(max < sum) {
					max = sum;
				}
			}
			br.close();
			isr.close();

			if(max == 0) {
				max = nMax;
			}
			System.out.println(max);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private int maxSerializedSum(final int[] serial) {
		int sum = 0;
		int max = 0;
		for(int i = 0; i < serial.length; i++) {
			if(serial[i] >= 0) {
				sum += serial[i];
			} else {
				if(sum + serial[i] >= 0) {
					sum += serial[i];
				} else {
					sum = 0;
				}
			}
			if(max < sum)
			{
				max = sum;
			}
		}

		if(max == 0) {
			int target = serial[0];
			for(int i = 1; i < serial.length;i++) {
				if(target < serial[i]) {
					target = serial[i];
				}
			}
			max = target;
		}
		return max;
	}

	public static void main(String[] args) {
		new Main();
	}
}
