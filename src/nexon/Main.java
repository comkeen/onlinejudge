package nexon;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

public class Main
{
	public static void main(String[] args)
	{
		new Main();
	}
	
	public Main()
	{
//		run();
	}
	
	private void run()
	{
	
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			
			String top = reader.readLine();
			int cases = Integer.parseInt(top);
			
			String[] inputs = new String[cases];
			for(int i = 0; i < cases; i++)
			{
				inputs[i] = reader.readLine();
			}
			reader.close();
			
			for(int i = 0; i < inputs.length; i++)
			{
				String[] temps = inputs[i].split(" ");
				solve();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public void test()
	{
		String input = "";
		String expected = "";
		
		assertEquals(input, expected);
	}
	
	private void solve()
	{
		
	}

}
