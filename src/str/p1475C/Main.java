package str.p1475C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			reader.close();
			
			solve(top);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(String input)
	{
		char[] inputs = input.toCharArray();
		int[] result = new int[10];
		int start = '0';
		for(int i = 0; i < input.length(); i++)
		{
			int temp = inputs[i];
			result[temp - start] += 1;
		}
		
		int max = (result[6]+result[9])/2 + (result[6]+result[9])%2;
		for(int i = 0; i < 10; i++)
		{
			if(i == 6 || i == 9)
			{
				continue;
			}
			if(max < result[i])
			{
				max = result[i];
			}
		}
		System.out.println(max);
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
