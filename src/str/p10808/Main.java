package str.p10808;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	
	public Main() {
		try	{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String input = reader.readLine();
			reader.close();

			solve(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void solve(String input) {
		char[] cInput = input.toCharArray();
		
		int[] count = new int['z'-'a'+1];
		for(int i = 0; i < cInput.length; i++) {
			count[cInput[i] - 'a']++;
		}
		
		for(int i = 0; i < 'z'-'a'+1; i++) {
			System.out.print(count[i] + " ");
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
