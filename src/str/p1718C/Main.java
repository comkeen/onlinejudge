package str.p1718C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			String key = reader.readLine();
			reader.close();
			solve(top, key);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(String input, String key)
	{
		char[] inputs = input.toCharArray();
		char[] keys = key.toCharArray();
		char[] result = new char[input.length()];
		
		int start = 'a';
		int end = 'z';
		int range = end - start + 1;
		for(int i = 0; i < input.length(); i++)
		{
			if(inputs[i] == ' ')
			{
				result[i] = ' ';
				continue;
			}
			int temp = inputs[i] - keys[i%key.length()] - 1;
			if(temp < 0)
			{
				temp = temp + range + start;
			}
			else
			{
				temp = temp + start;
			}
			result[i] = (char) temp;
		}
		
		for(int i = 0; i < result.length; i++)
		{
			System.out.print(result[i]);
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
