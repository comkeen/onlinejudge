package str.p1032C;

import java.util.Scanner;

public class Main
{

	public Main()
	{
		Scanner scanner = new Scanner(System.in);

		int testCases = Integer.parseInt(scanner.nextLine());
		String[] inputs = new String[testCases];
		for(int i = 0; i < testCases; i++)
		{
			String input = scanner.nextLine();
			inputs[i] = input;
		}

		String result = solve(inputs);
		System.out.println(result);
		scanner.close();
		return;
	}

	private String solve(String[] input)
	{
		String result = "";
		String temp = "";
		String target = "";
		for(int i = 0; i < input[0].length(); i++)
		{
			temp = input[0].substring(i, i+1);
			for(int j = 1; j < input.length; j++)
			{
				target = input[j].substring(i, i+1);
				if(!temp.equals(target))
				{
					temp = "?";
					break;
				}
			}
			result += temp;
		}
		return result;
	}


	public static void main(String[] args)
	{
		new Main();
	}
}
