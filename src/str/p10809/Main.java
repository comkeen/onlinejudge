package str.p10809;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {
	
	public Main() {
		try	{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String input = reader.readLine();
			reader.close();

			solve(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void solve(String input) {
		char[] cInput = input.toCharArray();
		
		int[] firstIndexs = new int['z'-'a'+1];
		Arrays.fill(firstIndexs, -1);
		
		for(int i = 0; i < cInput.length; i++) {
			if(firstIndexs[cInput[i] - 'a'] == -1) {
				firstIndexs[cInput[i] - 'a'] = i;
			}
		}
		
		for(int i = 0; i < 'z'-'a'+1; i++) {
			System.out.print(firstIndexs[i] + " ");
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
