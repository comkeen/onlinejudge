package str.p5525C;

import org.junit.Test;

public class TestRange
{
	private int N;
	private int M;
	private char[] pattern;

	@Test
	public void test()
	{
		N = 450000;
		M = 1000000;
		
		pattern = new char[2];
		pattern[0] = 'O';
		pattern[1] = 'I';
		
		String input = generateInput(M);
		
		double start = (double) System.currentTimeMillis()/1000;
		solve_charArr(input);
		double mid = (double) System.currentTimeMillis()/1000;
		System.out.println("charArr:" + (mid - start));
		solve_StringBuffer(input);
		double mid2 = (double) System.currentTimeMillis()/1000;
		System.out.println("stringbuffer:"+ (mid2 - mid));
		solve_StringBuilder(input);
		double mid3 = (double) System.currentTimeMillis()/1000;
		System.out.println("stringbuilder:"+ (mid3 - mid2));
		solve_charAt(input);
		double end = (double) System.currentTimeMillis()/1000;
		System.out.println("charAt:"+ (end - mid3));
	}

	private String generateInput(int M)
	{
		String temp = "IO";
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < M/2; i++)
		{
			sb.append(temp);
		}
		sb.append("I");
		return sb.toString();
	}

	private void solve_charArr(String input)
	{
		if(N > (M-1)/2)
		{
			System.out.println(0);
		}
		char[] cInput = input.toCharArray();
		
		char first = pattern[1];
		int sum = 0;
		int count = 0;
		for(int i = 0; i < M - N*pattern.length;) {	
			if(first == cInput[i]) {
				for(int j = 1; j <= M-1; j++) {
					if(pattern[(j-1)%pattern.length] == cInput[i+j]) {
						count++;
					} else {
						break;
					}
				}
				if(count/pattern.length >= N) {
					sum += count/pattern.length - N + 1;
				}
				i += count + 1;
				count = 0;
			} else {
				i++;
			}
		}
		System.out.println(sum);
	}
	
	private void solve_StringBuffer(String input)
	{
		if(N > (M-1)/2)
		{
			System.out.println(0);
		}
		StringBuffer sb = new StringBuffer(input);
		char first = pattern[1];
		int sum = 0;
		int count = 0;
		for(int i = 0; i < M - N*pattern.length;) {	
			if(first == sb.charAt(i)) {
				for(int j = 1; j <= M-1; j++) {
					if(pattern[(j-1)%pattern.length] == sb.charAt(i+j)) {
						count++;
					} else {
						break;
					}
				}
				if(count/pattern.length >= N) {
					sum += count/pattern.length - N + 1;
				}
				i += count + 1;
				count = 0;
			} else {
				i++;
			}
		}
		System.out.println(sum);
	}
	
	private void solve_StringBuilder(String input)
	{
		if(N > (M-1)/2)
		{
			System.out.println(0);
		}
		StringBuilder sb = new StringBuilder(input);
		char first = pattern[1];
		int sum = 0;
		int count = 0;
		for(int i = 0; i < M - N*pattern.length;) {	
			if(first == sb.charAt(i)) {
				for(int j = 1; j <= M-1; j++) {
					if(pattern[(j-1)%pattern.length] == sb.charAt(i+j)) {
						count++;
					} else {
						break;
					}
				}
				if(count/pattern.length >= N) {
					sum += count/pattern.length - N + 1;
				}
				i += count + 1;
				count = 0;
			} else {
				i++;
			}
		}
		System.out.println(sum);
	}
	
	private void solve_charAt(String input)
	{
		if(N > (M-1)/2)
		{
			System.out.println(0);
		}
		
		char first = pattern[1];
		int sum = 0;
		int count = 0;
		for(int i = 0; i < M - N*pattern.length;) {	
			if(first == input.charAt(i)) {
				for(int j = 1; j <= M-1; j++) {
					if(pattern[(j-1)%pattern.length] == input.charAt(i+j)) {
						count++;
					} else {
						break;
					}
				}
				if(count/pattern.length >= N) {
					sum += count/pattern.length - N + 1;
				}
				i += count + 1;
				count = 0;
			} else {
				i++;
			}
		}
		System.out.println(sum);
	}
}
