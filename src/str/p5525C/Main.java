package str.p5525C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	private int N;
	private int M;
	private char[] pattern;
	private char[] cInput;
	private char first;


	public Main()
	{
		try 
		{
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			N = Integer.parseInt(br.readLine());
			M = Integer.parseInt(br.readLine());
			String input = br.readLine();
			br.close();
			isr.close();

			pattern = new char[2];
			pattern[0] = 'O';
			pattern[1] = 'I';
			first = pattern[1];


			solve(input);
		}
		catch (NumberFormatException | IOException e) 
		{
			e.printStackTrace();
		}
	}

	private void solve(String input) {
		if(N > (M-1)/2)	{
			System.out.println(0);
		}
		cInput = input.toCharArray();

		int sum = 0;
		int count = 0;
		for(int i = 0; i < M - N*pattern.length;) {
			if(first == cInput[i]) {
				for(int j = 1; j <= M-1; j++) {
					if((i+j < M) && pattern[(j-1)%pattern.length] == cInput[i+j]) {
						count++;
					} else {
						break;
					}
				}
				if(count/pattern.length >= N) {
					sum += count/pattern.length - N + 1;
				}
				i += count + 1;
				count = 0;
			} else {
				i++;
			}
		}
		System.out.println(sum);
	}

	public static void main(String[] args) {
		new Main();
	}
}
