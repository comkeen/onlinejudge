package str.p9935C;

import java.util.Scanner;
import java.util.Stack;

public class Main
{
	Stack<Character> stack;

	public Main()
	{
		Scanner scanner = new Scanner(System.in);

		String input = scanner.nextLine();
		String target = scanner.nextLine();

		String result = solve(input, target);
		System.out.println(result);
		scanner.close();
		return;
	}

	private String solve(String input, String target)
	{
		stack = new Stack<Character>();

		for(int i = input.length() - 1; i >= 0; i--)
		{
			stack.push(input.charAt(i));
			if(stack.size() >= target.length() && stack.peek() == target.charAt(0))
			{
				boolean isTarget = false;
				for(int j = 1; j < target.length(); j++)
				{
					if(stack.get(stack.size()-1-j) != target.charAt(j))
					{
						isTarget = true;
						break;
					}
				}
				if(!isTarget)
				{
					for(int j = 0; j < target.length(); j++)
					{
						stack.pop();
					}
				}
			}
		}
		
		int size = stack.size();
		StringBuffer sb = new StringBuffer();
		
		if(stack.isEmpty())
		{
			System.out.println("FRULA");
		}
		else
		{
			for(int i = 0; i < size; i++)
			{
				sb.append(stack.pop());
			}
		}
		return sb.toString();
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
