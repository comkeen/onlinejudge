package str.p5598C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			reader.close();
			
			solve(top);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(String input)
	{
		char[] inputs = input.toCharArray();
		
		int start = 'A';
		int end = 'Z';
		int range = end - start + 1;
		char[] encoder = new char[range];
		for(int i = 0; i < range; i++)
		{
			int temp = start + i + 3;
			if(temp <= end)
			{
				encoder[i] = (char) temp;
			}
			else
			{
				encoder[i] = (char) (temp - range);
			}
		}
		
		for(int i = 0; i < inputs.length; i++)
		{
			int temp = inputs[i]-start-6;
			if(temp < 0)
			{
				temp = temp + range;
			}
			System.out.print(encoder[temp]);
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
