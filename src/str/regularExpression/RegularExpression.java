package str.regularExpression;

import static org.junit.Assert.assertEquals;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

public class RegularExpression
{
	String input;
	String expected;
	
	@Before
	public void init()
	{
		this.input = "aaaaabbbccrrrrrrweqaa";
		this.expected = "a5b3c2r6weqa2";
	}
	
	@Test
	public void testStringTokenizer() {
		String input = "a b cdf     dfdfdf a  b";
		String delimetor = " ";
		StringTokenizer st = new StringTokenizer(input, delimetor);
	}
	
//	@Test
	public void testCharacterArray()
	{
		StringBuffer sb = new StringBuffer();
		
		int count = 1;
		char[] cInput = input.toCharArray();
		char temp = cInput[0];
		for(int i = 1; i < input.length(); i++)
		{
			if(temp == cInput[i])
			{
				count++;
			}
			else 
			{
				sb.append(temp);
				if(count > 1)
				{
					sb.append(Integer.toString(count));
				}
				temp = cInput[i];
				count = 1;
			}
			if(i == input.length()-1)
			{
				sb.append(temp);
				if(count > 1)
				{
					sb.append(Integer.toString(count));
				}
			}
		}

		System.out.println(sb.toString());
		assertEquals(sb.toString(), expected);
	}

	@Test
	public void testRegularExpression(){
		Pattern p = Pattern.compile("(\\w)(\\1*)");
		Matcher m = p.matcher(input);
		StringBuffer sb = new StringBuffer();
		while(m.find()){
			sb.append(input.charAt(m.start()));
			if(m.end()-m.start() > 1){
				sb.append("" + (m.end()-m.start()));
			}
		}
		System.out.println(sb.toString());
		assertEquals(sb.toString(), expected);
	}
}
