package str.p9933C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String top = reader.readLine();
			int cases = Integer.parseInt(top);
			String[] inputs = new String[cases];
			for(int i = 0; i < cases; i++)
			{
				inputs[i] = reader.readLine();
			}
			reader.close();
			
			solve(inputs);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(String[] input)
	{
		for(int i = 0; i < input.length; i++)
		{
			String temp = input[i];
			char last = temp.charAt(temp.length()-1);
			for(int j = 0; j < input.length; j++)
			{
				String target = input[j];
				if(temp.length() == target.length() && last == target.charAt(0))
				{
					char[] temps = temp.toCharArray();
					char[] targets = target.toCharArray();
					boolean flag = true;
					for(int k = 1; k < targets.length; k++)
					{
						if(targets[k] != temps[targets.length - k - 1])
						{
							flag = false;
							break;
						}
					}
					if(!flag)
					{
						continue;
					}
					else
					{
						System.out.print(targets.length + " " + targets[targets.length/2]);
						return;
					}
				}
			}
		}
	}
	
	public static void main(String[] args)
	{
		new Main();
	}
}
