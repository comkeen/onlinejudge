package _template;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public Main() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			int T = Integer.parseInt(br.readLine());
			for(int i = 0; i < T; i++) {
				String[] tokens = br.readLine().split(" ");
			}
			br.close();
			isr.close();
			
			solve();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new Main();
	}

	private void solve() {
		
	}
}
