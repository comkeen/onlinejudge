package sort.p10989C;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

	public static final int MAX = 10001;
	private int N;

	public Main() {
		sort();
	}

	private void sort() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			N = Integer.parseInt(br.readLine());
			final int[] inputs = new int[N];
			for(int i = 0; i < N; i++) {
				inputs[i] = Integer.parseInt(br.readLine());
			}
			br.close();
			isr.close();

			Arrays.sort(inputs);
			BufferedOutputStream bos = new BufferedOutputStream(System.out);
			for(int i = 0; i < N; i++) {
				bos.write((inputs[i]+"\n").getBytes());
			}
			bos.flush();


		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Main();
	}
}
