package sort.p3047C;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

	public Main() {
		sort();
	}

	private void sort() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			final String[] numbers = br.readLine().split(" ");
			final String order = br.readLine();
			final int[] inputs = new int[3];
			for(int i = 0; i < 3; i++) {
				inputs[i] = Integer.parseInt(numbers[i]);
			}
			br.close();
			isr.close();

			Arrays.sort(inputs);
			BufferedOutputStream bos = new BufferedOutputStream(System.out);
			for(int i = 0; i < 3; i++) {
				switch (order.charAt(i)) {
				case 'A':
					bos.write((inputs[0] + " ").getBytes());
					break;
				case 'B':
					bos.write((inputs[1] + " ").getBytes());
					break;
				case 'C':
					bos.write((inputs[2] + " ").getBytes());
					break;
				default:
					break;
				}
			}
			bos.flush();


		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Main();
	}
}
