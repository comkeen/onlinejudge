package _nos;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.junit.Test;


public class NOS2011
{
	@Test
	public void problem1()
	{
		final String input = "aBcD";
		final String lowerCase = input.toLowerCase();

		int sum = 0;
		char[] arr = lowerCase.toCharArray();
		for(int i = 0; i < arr.length; i++)
		{
			int index = arr[i] - 'a' + 1;
			sum += index;
		}
		assertEquals(sum, 10);
	}

	@Test
	public void problem2()
	{
		final String input = "babbabbaby is babybababy";
		final String bab = "bab";
		final String baby = "baby";

		String[] tokens = input.split(" ");

		int result = 50;
		for(int i = 0; i < tokens.length; i++)
		{
			String temp = tokens[i];
			if(temp.substring(temp.length()-1).equals("b") || temp.substring(temp.length()-1).equals("y"))
			{
				if(temp.substring(temp.length()-3).equals(bab))
				{
					result += 1;
				}
				else if(temp.substring(temp.length()-4).equals(baby))
				{
					result -= 2;
				}
			}
		}
		assertEquals(result, 46);
	}

	@Test
	public void problem3()
	{
		final int n = 5;
		final String[] input = { "1 1 2", "2 1 3", "3 0 0", "4 2 2 3", "5 1 4"};
		//		final String[] input = { "1 4 2 3 4 5", "2 4 1 3 4 5", "3 4 1 2 4 5", "4 4 1 2 3 5", "5 4 1 2 3 4"};


		final int[][] adjMatrix = new int[n][n];
		for(int i = 0; i < n; i++)
		{
			String[] tokens = input[i].split(" ");
			for(int j = 0; j < Integer.parseInt(tokens[1]); j++)
			{
				adjMatrix[i][Integer.parseInt(tokens[1+j])] = adjMatrix[Integer.parseInt(tokens[1+j])][i] = 1;
			}
		}

		List<int[]> players = new ArrayList<>();
		for(int i = 0; i < n; i++)
		{
			Queue<Integer> queue = new LinkedList<>();
			final int[] player = new int[n];
			final int[] eliminate = new int[n];
			
			int counter = 0;
			player[i] = 1;		
			queue.offer(i);		
			while(!queue.isEmpty())
			{
				int temp = queue.poll();
				for(int j = 0; j < n; j++)
				{
					if(adjMatrix[temp][j] == 1 && eliminate[j] != 1)
					{
						if(player[j] == 0)
						{
							player[j] = -player[temp];
							counter++;
							queue.offer(j);
						}
						else
						{
							if(player[temp] == player[j])
							{
								eliminate[j] = 1;
							}
						}
						
					}
				}
			}
			if(counter >= 2)
			{
				if(!isContainsArray(players, player))
				{
					players.add(player);
				}
			}
		}
		System.out.println(players.size());

		for (int[] player : players)
		{
			List<Integer> A = new ArrayList<>();
			List<Integer> B = new ArrayList<>();
			for(int i = 0; i < player.length; i++)
			{
				if(player[i] == 1)
				{
					A.add(i+1);
				}
				else if(player[i] == -1)
				{
					B.add(i+1);
				}
			}
			if(A.isEmpty() || B.isEmpty())
			{
				System.out.println();
				continue;
			}
			printAB(A, B);
		}
	}

	private boolean isContainsArray(List<int[]> players, int[] player)
	{
		for(int[] arr : players)
		{
			if(Arrays.equals(arr, player))
			{
				return true;
			}
		}
		return false;
	}

	private void printAB(List<Integer> a, List<Integer> b)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("A팀 : ");
		sb.append(a.get(0));
		for(int i = 1; i < a.size(); i++)
		{
			sb.append(",").append(a.get(i));
		}
		sb.append("\nB팀 : ");
		sb.append(b.get(0));
		for(int i = 1; i < b.size(); i++)
		{
			sb.append(",").append(b.get(i));
		}
		System.out.println(sb.toString());
	}
}
