package _nos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import org.junit.Test;

public class NOS2006
{
	@Test
	public void problem1()
	{
		Set<Integer> hasGeneratorSet = new HashSet<>();
		for(int i = 1; i < 5000; i++)
		{
			int num = i + i/1000 + i/100%10 + i/10%10 + i%10;
			hasGeneratorSet.add(num);
		}

		List<Integer> selfNumberList = new ArrayList<>();
		int sum = 0;
		for(int i = 1; i < 5000; i++)
		{
			if(!hasGeneratorSet.contains(i))
			{
				sum += i;
				selfNumberList.add(i);
			}
		}
		//		System.out.println(sum);
	}

	@Test
	public void problem2()
	{
		final int row = 8;
		final int column = 8;

		final String mapValue = "01010101 11111011 01011010 11111111 11101111 11111010 10111111 11110101";
		final String[] tokens = mapValue.split(" ");

		final int[][] map = new int[8][8];
		for(int i = 0; i < row; i++)
		{
			String temp = tokens[i];
			for(int j = 0; j < column; j++)
			{
				map[i][j] = (Integer.parseInt(temp.substring(j, j+1)));
			}
		}

		int[][] placedMap = map;
		int counter = 0;
		for(int i = 0; i < row; i++)
		{
			for(int j = 0; j < column; j++)
			{
				if(canPlace(placedMap, i, j))
				{
					placedMap[i][j] = 2;
					counter++;
//					System.out.println(counter);
				}
			}
		}
	}

	private boolean canPlace(int[][] map, int row, int column)
	{
		int[] vertical = {-1,0,1,0};
		int[] horizontal = {0,-1,0,1};
		findPath:for(int i = 0; i < 4; i++)
		{
			int nextRow = row+vertical[i];
			int nextColumn = column+horizontal[i];
			while(nextRow >= 0 && nextRow < map.length && nextColumn >= 0 && nextColumn < map[0].length)
			{
				if(map[nextRow][nextColumn] == 2)
				{
					return false;
				}
				if(map[nextRow][nextColumn] == 0)
				{
					continue findPath;
				}
				nextRow = nextRow+vertical[i];
				nextColumn = nextColumn+horizontal[i];
			}
		}
		return true;
	}

	@Test
	public void problem3()
	{
		final int[] nums = 
			{
					49, 49, 50, 52,   49, 47, 47, 46,   44, 42, 42, 38,   38, 38, 36, 34,
					33, 33, 33, 32,   34, 38, 42, 41,   42, 42, 40, 41,   40, 38, 38, 37,
					37, 39, 41, 40,   40, 40, 40, 42,   45, 47, 47, 46,   46, 46, 47, 47,
					47, 47, 46, 44,   43, 39, 36, 34,   34, 32, 30, 29,   30, 31, 31, 31,
					30, 28, 25, 23,   24, 22, 22, 25,   25, 27, 31, 33,   35, 37, 38, 39,
					39, 40, 40, 41,   40, 40, 40, 40,   39, 38, 37, 35,   35, 34, 33, 32,
					31, 30, 30, 29,   29, 28, 27, 27,   28, 27, 25, 26,   0, 0, 90, 90,
					90, 90, 90, 91,   90, 88, 87, 84,   80, 78, 83, 89,   91, 90, 89, 92
			};
		solve1(nums);
	}

	private void solve1(int[] nums)
	{
		final int division = 7;

		PriorityQueue<Difference> topDiffernce = new PriorityQueue<>(division);

		for(int i = 0; i < nums.length-1; i++)
		{
			int difference = Math.abs(nums[i] - nums[i+1]);
			if(topDiffernce.size() < division)
			{
				topDiffernce.offer(new Difference(i, difference));
			}
			else
			{
				if(topDiffernce.peek().difference < difference)
				{
					topDiffernce.poll();
					topDiffernce.offer(new Difference(i, difference));
				}
			}
		}
		System.out.println(topDiffernce);
		
		List<Integer> indexs = new ArrayList<>();
		int size = topDiffernce.size();
		Difference init = topDiffernce.poll();
		while(!topDiffernce.isEmpty())
		{
			Difference temp = topDiffernce.poll();
			if(init.difference < temp.difference)
			{
				indexs.add(temp.index);
			}
		}
		indexs.add(nums.length-1);
		Collections.sort(indexs);
		for(int i = 0; i < 8-indexs.size(); i++)
		{
			indexs.add(findIndex(indexs.get(0)));
		}
		
		System.out.println(indexs);
		
		int pointer = 0;
		List<RLE> results = new ArrayList<>();
		for(int i = 0; i < indexs.size(); i++)
		{
			int sum = 0;
			for(int j = pointer; j < indexs.get(i) + 1; j++)
			{
				sum += nums[j];
			}
			int numbers = indexs.get(i) - pointer + 1;
			int average = sum/numbers;
			results.add(new RLE(numbers, average));
			pointer = indexs.get(i) + 1;
		}
		System.out.println(results);
//		printRMSE(nums, results);
		int[] rle = RLEtoArray(results);
		printRMSE(nums, rle);
	}

	private Integer findIndex(Integer integer) {
		// TODO Auto-generated method stub
		return null;
	}

	private int[] RLEtoArray(List<RLE> list)
	{
		int[] results = new int[128];
		int pointer = 0;
		for(int i = 0; i < list.size(); i++)
		{
			for(int j = 0; j < list.get(i).numbers; j++)
			{
				results[pointer] = list.get(i).value;
				pointer++;
			}
		}
		return results;
	}
	
	private void printRMSE(int[] nums, int[] rle)
	{
		int squareSum = 0;
		for(int i = 0; i < nums.length; i++)
		{
			int dif = nums[i] - rle[i];
			squareSum += dif*dif;
		}
		double rmse = Math.sqrt(squareSum/nums.length);
		System.out.println(rmse);
	}
	
	private void printRMSE(int[] nums, List<RLE> list)
	{
		int pointer = 0;
		int squareSum = 0;
		double rmseSum = 0.0f;
		for(int i = 0; i < list.size(); i++)
		{
			squareSum = 0;
			for(int j = 0; j < list.get(i).numbers; j++)
			{
				int dif = nums[pointer] - list.get(i).value;
				squareSum += dif*dif;
				pointer++;
			}
			rmseSum += Math.sqrt(squareSum/list.get(i).numbers);
		}
//		double rmse = Math.sqrt(squareSum/nums.length);
//		System.out.println(rmse);
		System.out.println(rmseSum/8);
	}

	public class Difference implements Comparable<Difference>
	{
		public int index;
		public int difference;

		public Difference(int index, int difference)
		{
			this.index = index;
			this.difference = difference;
		}
		
		@Override
		public String toString()
		{
			return this.difference + "";
		}

		@Override
		public int compareTo(Difference o)
		{
			return difference - o.difference;
		}
	}
	
	public class RLE
	{
		public int numbers;
		public int value;
		
		public RLE(int numbers, int value)
		{
			this.numbers = numbers;
			this.value = value;
		}
		
		public String toString()
		{
			return "(" + numbers + "," + value + ")";
		}
	}
}
