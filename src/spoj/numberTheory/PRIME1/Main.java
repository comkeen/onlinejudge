package spoj.numberTheory.PRIME1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

	public Main() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			String[] tokens = br.readLine().split("");
			int N = Integer.parseInt(br.readLine());
			br.close();
			isr.close();
			
			int[] arr = new int[N+1];
			for(int i = 2; i < N; i++) {
				arr[i] = i;
			}
			
			for(int i = 2; i < Math.sqrt(N); i++) {
				if(arr[i] == 0) {
					continue;
				}
				for(int j = i + i; j <= N; j+=i) {
					arr[j] = 0;
				}
			}
			System.out.println(Arrays.toString(arr));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new Main();
	}
}
