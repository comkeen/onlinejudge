package uni.p11285C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	private String[] inits = {"ㄱ", "ㄲ", "ㄴ", "ㄷ", "ㄸ", "ㄹ", "ㅁ", "ㅂ", "ㅃ", "ㅅ", "ㅆ", "ㅇ", "ㅈ", "ㅉ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ"};
	private String[] mids = {"ㅏ", "ㅐ", "ㅑ", "ㅒ", "ㅓ", "ㅔ", "ㅕ", "ㅖ", "ㅗ", "ㅘ", "ㅙ", "ㅚ", "ㅛ", "ㅜ", "ㅝ", "ㅞ", "ㅟ", "ㅠ", "ㅡ", "ㅢ", "ㅣ"};
	private String[] lasts = {" ", "ㄱ", "ㄲ", "ㄳ", "ㄴ", "ㄵ", "ㄶ", "ㄷ", "ㄹ", "ㄺ", "ㄻ", "ㄼ", "ㄽ", "ㄾ", "ㄿ", "ㅀ", "ㅁ", "ㅂ", "ㅄ", "ㅅ", "ㅆ", "ㅇ", "ㅈ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ"};
	
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String init = reader.readLine();
			String mid = reader.readLine();
			String last = reader.readLine();

			solve(init, mid, last);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void solve(String init, String mid, String last)
	{
		int initI = findInitIndex(init);
		int midI = findMidIndex(mid);
		int lastI = findLastIndex(last);
		int result = '가' + initI*21*28 + midI*28 + lastI;
		System.out.println((char)result);
	}
	
	private int findInitIndex(String init)
	{
		int initI = -1;
		for(int i = 0; i < inits.length; i++)
		{
			if(init.equals(inits[i]))
			{
				initI = i;
			}
		}
		return initI;
	}

	private int findMidIndex(String mid)
	{
		int midI = -1;
		for(int i = 0; i < mids.length; i++)
		{
			if(mid.equals(mids[i]))
			{
				midI = i;
			}
		}
		return midI;
	}

	private int findLastIndex(String last)
	{
		if(last.equals(" ") || last.length() == 0)
		{
			return 0;
		}
		else
		{
			int lastI = -1;
			for(int i = 0; i < lasts.length; i++)
			{
				if(last.equals(lasts[i]))
				{
					lastI = i;
				}
			}
			return lastI;
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
