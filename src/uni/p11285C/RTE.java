package uni.p11285C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RTE
{
	private char[] inits = {'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ'};
	private char[] lasts = {' ', 'ㄱ', 'ㄲ', 'ㄳ', 'ㄴ', 'ㄵ', 'ㄶ', 'ㄷ', 'ㄹ', 'ㄺ', 'ㄻ', 'ㄼ', 'ㄽ', 'ㄾ', 'ㄿ', 'ㅀ', 'ㅁ', 'ㅂ', 'ㅄ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ'};
	public RTE()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String init = reader.readLine();
			String mid = reader.readLine();
			String last = reader.readLine();

			solve(init, mid, last);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void solve(String init, String mid, String last)
	{
		int initI = findInitIndex(init.charAt(0));
		int midI = findMidIndex(mid.charAt(0));
		int lastI = findLastIndex(last);
		int result = '가' + initI*21*lasts.length + midI*lasts.length + lastI;
		System.out.println((char) result);
	}

	private int findInitIndex(char init)
	{
		int initI = -1;
		for(int i = 0; i < inits.length; i++)
		{
			if(init == inits[i])
			{
				initI = i;
			}
		}
		return initI;
	}

	private int findMidIndex(char mid)
	{
		return (int) (mid - 'ㅏ');
	}

	private int findLastIndex(String last)
	{
		if(last.equals(" "))
		{
			return 0;
		}
		else
		{
			int lastI = -1;
			for(int i = 1; i < lasts.length; i++)
			{
				if(last.charAt(0) == lasts[i])
				{
					lastI = i;
				}
			}
			return lastI;
		}
	}

	public static void main(String[] args)
	{
		new RTE();
	}
}
