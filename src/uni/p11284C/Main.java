package uni.p11284C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	private String[] inits = {"ㄱ", "ㄲ", "ㄴ", "ㄷ", "ㄸ", "ㄹ", "ㅁ", "ㅂ", "ㅃ", "ㅅ", "ㅆ", "ㅇ", "ㅈ", "ㅉ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ"};
	private String[] mids = {"ㅏ", "ㅐ", "ㅑ", "ㅒ", "ㅓ", "ㅔ", "ㅕ", "ㅖ", "ㅗ", "ㅘ", "ㅙ", "ㅚ", "ㅛ", "ㅜ", "ㅝ", "ㅞ", "ㅟ", "ㅠ", "ㅡ", "ㅢ", "ㅣ"};
	private String[] lasts = {" ", "ㄱ", "ㄲ", "ㄳ", "ㄴ", "ㄵ", "ㄶ", "ㄷ", "ㄹ", "ㄺ", "ㄻ", "ㄼ", "ㄽ", "ㄾ", "ㄿ", "ㅀ", "ㅁ", "ㅂ", "ㅄ", "ㅅ", "ㅆ", "ㅇ", "ㅈ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ"};

	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String str = reader.readLine();

			solve(str);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void solve(String str)
	{
		int initI = findInitIndex(str.charAt(0));
		int midI = findMidIndex(str.charAt(0));
		int lastI = findLastIndex(str.charAt(0));
		
		System.out.println(inits[initI]);
		System.out.println(mids[midI]);
		System.out.println(lasts[lastI]);
	}

	private int findInitIndex(char code)
	{
		return (code-'가')/21/28;
	}
	
	private int findMidIndex(char code)
	{
		return (code-'가')/28%21;
	}

	private int findLastIndex(char code)
	{
		return (code-'가')%28;
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
