package uni.p11283C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String n = reader.readLine();

			solve(n.trim());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(String n)
	{
		int result = n.charAt(0) - '가';
		System.out.println(result+1);
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
