package uni.p11282C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			int n = Integer.parseInt(reader.readLine()) - 1;

			solve_simple(n);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve_simple(int n)
	{
		char result = (char) ('가' + n);
		System.out.println(result);
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
