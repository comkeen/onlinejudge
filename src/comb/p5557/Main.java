package comb.p5557;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main
{
	public Main()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			int nums = Integer.parseInt(reader.readLine());

			int[] inputs = new int[nums];
			for(int i = 0; i < nums; i++)
			{
				inputs[i] = Integer.parseInt(reader.readLine());
			}
			reader.close();
			solve(inputs);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void solve(int[] inputs)
	{
		int[][] dp = new int[100][100];
		dp[0][0] = inputs[0];
		for(int i = 1; i < inputs.length - 1; i++)
		{
			for(int j = 1; j <= sqaure(i); j++)
			{
				dp[i][j-1] = dp[i-1][j] + inputs[i];
				dp[i][j] = dp[i-1][j] - inputs[i];
			}
		}
	}

	private int sqaure(int n)
	{
		int result = 1;
		for(int i = 1; i <= n; i++)
		{
			result = 2*result;
		}
		return result;
	}
	
	public static void main(String[] args)
	{
		new Main();
	}
}
