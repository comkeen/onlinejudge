package problem.p5074C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main
{
	public static final String END_POINT = "00:00 00:00";

	private enum State{
		hour, min
	}

	public Main()
	{
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			List<String> inputList = new ArrayList<>();
			String temp;
			temp = br.readLine();
			while(temp != null)
			{
				if(END_POINT.equals(temp))
				{
					break;
				}
				inputList.add(temp);
				temp = br.readLine();
			}
			br.close();
			isr.close();

			String result = "";
			for(int i = 0; i < inputList.size(); i ++)
			{
				String[] tokens = inputList.get(i).split(" ");
				result += calculateEndTime(tokens[0], tokens[1]) + "\n";
			}

			System.out.println(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String calculateEndTime(String start, String during)
	{
		String[] startTime = start.split(":");
		int startHInt = Integer.parseInt(startTime[0]);
		int startMInt = Integer.parseInt(startTime[1]);

		String[] duringTime = during.split(":");
		int duringHInt = Integer.parseInt(duringTime[0]);
		int duringMInt = Integer.parseInt(duringTime[1]);

		int[] minuteArr = add(startMInt, duringMInt, State.min);
		int[] hourArr = add(startHInt + minuteArr[1], duringHInt, State.hour);

		String result = String.format("%02d", hourArr[0]) + ":" + String.format("%02d", minuteArr[0]);
		if(hourArr[1] != 0)
		{
			result += " +" + String.format("%01d", hourArr[1]);
		}

		return result;
	}

	private int[] add(int left, int right, State state)
	{
		int resultInt = left + right;

		int[] results = new int[2];
		switch (state) {
		case hour:
			results[0] = resultInt%24;
			results[1] = resultInt/24;
			break;
		case min:
			results[0] = resultInt%60;
			results[1] = resultInt/60;
			break;
		default:
			break;
		}
		return results;
	}

	public static void main(String[] args)
	{       
		new Main();
	}
}