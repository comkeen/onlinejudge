package problem.p2079;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class Main
{
	@Test
	public void test()
	{
		String input = "anavolimilana";
		String expected = "anavolimilana";

		findPalindrome(input);
		assertEquals(input, expected);
	}


	private void findPalindrome(String input)
	{
		Map<String, List<Integer>> palindromeAndIndex = new HashMap<>();

		char[] cInput = input.toCharArray();
		for(int i = 0; i < cInput.length; i++)
		{
			if(palindromeAndIndex.containsKey(String.valueOf(cInput[i])))
			{
				palindromeAndIndex.get(String.valueOf(cInput[i])).add(i);
			}
			else
			{
				List<Integer> indexs = new ArrayList<>(cInput.length);
				indexs.add(i);
				palindromeAndIndex.put(String.valueOf(cInput[i]), indexs);
				
			}
		}

	}
}
