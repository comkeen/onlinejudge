package problem.p1004C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public Main() {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			int T = Integer.parseInt(reader.readLine());
			int[] results = new int[T];
			for(int i = 0; i < T; i++) {
				String[] tokens = reader.readLine().split(" ");
				int startX = Integer.parseInt(tokens[0]);
				int startY = Integer.parseInt(tokens[1]);
				int goalX = Integer.parseInt(tokens[2]);
				int goalY = Integer.parseInt(tokens[3]);
				int planetNumber = Integer.parseInt(reader.readLine());
				String[] planets = new String[planetNumber];
				for(int j = 0; j < planetNumber; j++) {
					planets[j] = reader.readLine();
				}
				results[i] = solve(startX, startY, goalX, goalY, planets);
			}
			reader.close();
			
			for(int i = 0; i < T; i++)
			{
				System.out.println(results[i]);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new Main();
	}

	private int solve(int startX, int startY, int goalX, int goalY, String[] planets) {
		boolean[] isStartIn = new boolean[planets.length];
		boolean[] isGoalIn = new boolean[planets.length];
		for(int i = 0; i < planets.length; i++) {
			if(isInside(startX, startY, planets[i])){
				isStartIn[i] = true; 
			}
			if(isInside(goalX, goalY, planets[i])){
				isGoalIn[i] = true; 
			}
		}
		int sum = 0;
		for(int i = 0; i < planets.length; i++) {
			if(isStartIn[i]&isGoalIn[i])
			{
				continue;
			}
			if(isStartIn[i])
			{
				sum++;
			}
			if(isGoalIn[i])
			{
				sum++;
			}
		}
		return sum;
	}
	private boolean isInside(int x, int y, String planet) {
		String[] tokens = planet.split(" ");
		int pX = Integer.parseInt(tokens[0]);
		int pY = Integer.parseInt(tokens[1]);
		int pR = Integer.parseInt(tokens[2]);
		
		int xDist = (pX - x);
		int yDist = (pY - y);
		if(xDist*xDist + yDist*yDist <= pR*pR) {
			return true;
		}
		return false;
	}

}
