package problem.p10988C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	public Main()
	{
		try
		{
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			String input;
			input = br.readLine();
			int result = checkPalindrome(input);
			System.out.println(result);
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		new Main();
	}


	private int checkPalindrome(String input)
	{
		char[] target = input.toCharArray();
		for(int i = 0; i < target.length/2; i++)
		{
			if(Character.compare(target[i], target[target.length - i - 1]) != 0)
			{
				return 0;
			}
		}
		return 1;
	}
}
