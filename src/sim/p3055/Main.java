package sim.p3055;

import java.util.Scanner;


public class Main
{
	private int raw;
	private int column;
	private int tick;
	private int cost;
	private String[] trace;

	private int startX;
	private int startY;
	private int goalX;
	private int goalY;

	public static final String IMPOSSIBLE = "KAKTUS";
	public static final String START = "D";
	public static final String GOAL = "S";
	public static final String WATER = "*";
	public static final String ROCK = "X";
	public static final String EMPTY = ".";

	public Main()
	{
		Scanner scanner = new Scanner(System.in);

		String temp =  scanner.nextLine();
		String[] temps = temp.split(" ");
		raw = Integer.parseInt(temps[0]);
		column = Integer.parseInt(temps[1]);

		init();
		String[][] forest = new String[raw][column];
		for(int i = 0; i < raw; i++)
		{
			temp = scanner.nextLine();

			for(int j = 0; j < column; j++)
			{
				String token = temp.substring(j, j+1);
				forest[i][j] = token;
				if(token.equals(START))
				{
					startX = i;
					startY = j;
				}
				else if(token.equals(GOAL))
				{
					goalX = i;
					goalY = i;
				}
			}
		}
		solve(forest);
		scanner.close();
		return;
	}

	private void init()
	{
		trace = new String[250];
		cost = 0;
	}

	private void printForest(String[][] input)
	{
		for(int i = 0; i < raw; i++)
		{
			for(int j = 0; j < column; j++)
			{
				System.out.print(input[i][j]);				
			}
			System.out.println();
		}
	}

	private String solve(String[][] input)
	{
		tick = 0;
		while(tick < 4)
		{
			input = expandWater(input);		
			printForest(input);
			tick++;
		}
		return "";
	}

	private String[][] cloneArray(String[][] input)
	{
		String[][] clone = new String[raw][column];
		for(int i = 0; i < raw; i++)
		{
			for(int j = 0; j < column; j++)
			{
				clone[i][j] = input[i][j];
			}
		}
		return clone;
	}

	private String[][] expandWater(String[][] forest)
	{
		String[][] clone = cloneArray(forest);

		String temp, target;

		for(int i = 0; i < raw; i++)
		{
			for(int j = 0; j < column; j++)
			{
				temp = forest[i][j];
				if(temp.equals(WATER))
				{
					//up
					if(i - 1 >= 0)
					{
						target = forest[i - 1][j];
						if(target.equals(EMPTY))
						{
							clone[i-1][j] = WATER;						
						}
					}
					//down
					if(i + 1 < raw)
					{
						target = forest[i + 1][j];
						if(target.equals(EMPTY))
						{
							clone[i+1][j] = WATER;
						}
					}
					//left
					if(j - 1 >= 0)
					{
						target = forest[i][j-1];
						if(target.equals(EMPTY))
						{
							clone[i][j-1] = WATER;
						}
					}
					//right
					if(j + 1 < column)
					{
						target = forest[i][j+1];
						if(target.equals(EMPTY))
						{
							clone[i][j+1] = WATER;
						}
					}
				}
			}
		}
		return clone;
	}

	private String[][] move(String[][] forest)
	{
		int dirX, dirY;
		String target;
		//up
		if(startX - 1 >= 0)
		{
			target = forest[startX - 1][startY];
			if(target.equals(EMPTY))
			{
				forest[startX - 1][startY] = START;		
			}
		}
		return forest;
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
