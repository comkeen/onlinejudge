package sim.p1094C;

import java.util.Scanner;


public class Main
{

	public Main()
	{
		Scanner scanner = new Scanner(System.in);

		int testCase = scanner.nextInt();
		int result = solve(testCase);
		System.out.println(result);

		scanner.close();
		return;
	}

	private int solve(int input)
	{
		int temp = 64;
		int counter = 1;
		
		while(input != temp)
		{	
			temp = temp / 2;
			if(temp < input)
			{	
				input = input - temp;
				counter++;
			}
			else
			{
				continue;
			}
		}
		return counter;
	}

	public static void main(String[] args)
	{
		new Main();
	}
}
