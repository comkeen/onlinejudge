package test;

public class Test {
	
	public static final String NAME = "test";
	
	static {
		System.out.println("test");
	}
		
	public Test() {
		System.out.println("test.Test LOADED!!!");
	}
	
	public static void test() {
		System.out.println("test.test called");
	}
}
