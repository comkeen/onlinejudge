package test;

public enum TestEnum {
	INSTANCE, A, B, C;
	
	private String a = "FIVE";
	
	static TestEnum getInstance() {
		return INSTANCE;
	}

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}
	
}
