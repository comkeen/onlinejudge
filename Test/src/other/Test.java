package other;

public class Test {
	
	public static final String NAME = "other";
	
	static {
		System.out.println("other");
	}
	
	
	public Test() {
		System.out.println("other.Test LOADED!!!");
	}
	
	public static void test() {
		System.out.println("other.test called");
	}
}
