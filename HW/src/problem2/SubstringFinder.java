package problem2;

public class SubstringFinder
{
	public SubstringFinder()
	{
		String input = "ACCGGCCGAGACAGCGAGCATATGCCGCCGAGACAGGCCGGACCGGCCGAGA";
		String codon = "GCCG";
		
		findCodon(input, codon);
	}

	void findCodon(String acgtSequence, String codon)
	{
		char[] inputCharArray = acgtSequence.toCharArray();
		char[] targetCharArray = codon.toCharArray();
		int targetStartPoint = targetCharArray[0];
		
		for(int i = 0; i < inputCharArray.length; i++)
		{
			int temp = inputCharArray[i];
			//acgtSequence의 문자열과 codon의 첫번째 문자열 비교
			if(temp == targetStartPoint)
			{
				int count = 1;
				//codon의 문자열과 일치하는지 비교
				for(int j = 1; j < targetCharArray.length; j++)
				{
					//불일치할때
					if(inputCharArray[i+j] != targetCharArray[j])
					{
						i += j - 1;
						break;
					}
					count++;
				}
				//일치할때
				if(count == targetCharArray.length)
				{
					System.out.println(i);
					i += targetCharArray.length - 1;
				}
			}
		}
	}
	
	public static void main(String[] args)
	{
		new SubstringFinder();
	}
}
