package problem3;

import java.util.Scanner;

public class Test
{
	public static final int MAX_N = 45000;

	public Test()
	{
		Scanner scanner = new Scanner(System.in);
		int testCases = scanner.nextInt();
		int[] inputs = new int[testCases];
		for(int i = 0; i < testCases; i++)
		{
			inputs[i] = scanner.nextInt();
		}
		for(int i = 0; i < testCases; i++)
		{
			int result = serializedIntegerOperate(inputs[i]);
			System.out.println(result);
		}
		scanner.close();
	}

	private int serializedIntegerOperate(int input)
	{
		int[] serialiezedIntegers = new int[MAX_N];
		int sum = 0;
		int n = 0;
		int sign = 1;
		//입력값이 음수일 경우
		if(input < 0)
		{
			sign = -1;
			input = -input;
		}
		//입력값보다 큰 1의 등차합 구하기
		while(sum < input)
		{
			n++;
			serialiezedIntegers[n-1] = n * sign;
			sum += n;
		}
		int difference = sum - input;
		//등차합과 입력값이 같은 경우
		if(difference == 0)
		{
			//return n;
		}
		//등차합과 입력값의 차가 홀수인 경우
		else if((difference)%2 == 1)
		{
			//-1
			n++;
			sum += n;
			serialiezedIntegers[n-1] = n * sign;
			n++;
			sum += -n;
			serialiezedIntegers[n-1] = -(n * sign);
			difference += -1;

			//등차합과 입력값의 차에서 1을 뺀 후 차가 존재한다면
			if(difference != 0)
			{
				sum += -(serialiezedIntegers[((difference)/2)-1]*2*sign);
				serialiezedIntegers[((difference)/2)-1] = -serialiezedIntegers[((difference)/2)-1];
			}
			//return n;
		}
		//등차합과 입력값의 차가 짝수인 경우
		else if((difference)%2 == 0)
		{		
			sum += -(serialiezedIntegers[((difference)/2)-1]*2*sign);
			serialiezedIntegers[((difference)/2)-1] = -serialiezedIntegers[((difference)/2)-1];
			//return n;
		}
		sum = sum * sign;
		//검산
		if(checkSum(serialiezedIntegers, sum))
		{
			return n;
		}
		else
		{
			return 0;
		}
	}

	private boolean checkSum(int[] results, int input)
	{
		int result = 0;
		for(int i = 0; i < results.length; i++)
		{
			result += results[i];
		}
		if(result == input)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static void main(String[] args)
	{
		new Test();
	}
}
