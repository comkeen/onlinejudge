package ds;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class DataStructure
{
	@Test
	public void overwriteKeyTest()
	{
		final Map<String, String> preferences = new HashMap<>();
		preferences.put("like", "a");
		preferences.put("dislike", "b");
		
		Assert.assertEquals("a", preferences.get("like"));
		
		preferences.put("like", "c");
		
		Assert.assertEquals("c", preferences.get("like"));
	}

}
